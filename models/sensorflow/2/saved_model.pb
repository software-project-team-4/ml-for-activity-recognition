˘
˝11
:
Add
x"T
y"T
z"T"
Ttype:
2	
W
AddN
inputs"T*N
sum"T"
Nint(0"!
Ttype:
2	

ArgMax

input"T
	dimension"Tidx
output"output_type" 
Ttype:
2	"
Tidxtype0:
2	"
output_typetype0	:
2	
P
Assert
	condition
	
data2T"
T
list(type)(0"
	summarizeint
E
AssignAddVariableOp
resource
value"dtype"
dtypetype
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
~
BiasAddGrad
out_backprop"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
ě
Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)


Conv2DBackpropFilter

input"T
filter_sizes
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)


Conv2DBackpropInput
input_sizes
filter"T
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

š
DenseToDenseSetOperation	
set1"T	
set2"T
result_indices	
result_values"T
result_shape	"
set_operationstring"
validate_indicesbool("
Ttype:
	2	
S
DynamicStitch
indices*N
data"T*N
merged"T"
Nint(0"	
Ttype
B
Equal
x"T
y"T
z
"
Ttype:
2	

W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
?
FloorDiv
x"T
y"T
z"T"
Ttype:
2	
9
FloorMod
x"T
y"T
z"T"
Ttype:

2	
=
Greater
x"T
y"T
z
"
Ttype:
2	
B
GreaterEqual
x"T
y"T
z
"
Ttype:
2	
.
Identity

input"T
output"T"	
Ttype
?
	LessEqual
x"T
y"T
z
"
Ttype:
2	
,
Log
x"T
y"T"
Ttype:

2
p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2
;
Maximum
x"T
y"T
z"T"
Ttype:

2	

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
N
Merge
inputs"T*N
output"T
value_index"	
Ttype"
Nint(0
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
;
Minimum
x"T
y"T
z"T"
Ttype:

2	
=
Mul
x"T
y"T
z"T"
Ttype:
2	
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
X
PlaceholderWithDefault
input"dtype
output"dtype"
dtypetype"
shapeshape
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
a
Range
start"Tidx
limit"Tidx
delta"Tidx
output"Tidx"
Tidxtype0:	
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
5

Reciprocal
x"T
y"T"
Ttype:

2	
E
Relu
features"T
activations"T"
Ttype:
2	
V
ReluGrad
	gradients"T
features"T
	backprops"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
e
ShapeN
input"T*N
output"out_type*N"
Nint(0"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
=
SigmoidGrad
y"T
dy"T
z"T"
Ttype:

2
O
Size

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
9
Softmax
logits"T
softmax"T"
Ttype:
2
-
Sqrt
x"T
y"T"
Ttype:

2
1
Square
x"T
y"T"
Ttype:

2	
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
ö
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
:
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
M
Switch	
data"T
pred

output_false"T
output_true"T"	
Ttype
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape
9
VarIsInitializedOp
resource
is_initialized
"serve*1.12.02v1.12.0-0-ga6d8ffae09ˇ	
{
conv1d_3_inputPlaceholder*
dtype0*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ú*!
shape:˙˙˙˙˙˙˙˙˙ú
Š
0conv1d_3/kernel/Initializer/random_uniform/shapeConst*!
valueB"      @   *"
_class
loc:@conv1d_3/kernel*
dtype0*
_output_shapes
:

.conv1d_3/kernel/Initializer/random_uniform/minConst*
valueB
 *l-ž*"
_class
loc:@conv1d_3/kernel*
dtype0*
_output_shapes
: 

.conv1d_3/kernel/Initializer/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *l->*"
_class
loc:@conv1d_3/kernel
ň
8conv1d_3/kernel/Initializer/random_uniform/RandomUniformRandomUniform0conv1d_3/kernel/Initializer/random_uniform/shape*
dtype0*"
_output_shapes
:@*

seed *
T0*"
_class
loc:@conv1d_3/kernel*
seed2 
Ú
.conv1d_3/kernel/Initializer/random_uniform/subSub.conv1d_3/kernel/Initializer/random_uniform/max.conv1d_3/kernel/Initializer/random_uniform/min*
_output_shapes
: *
T0*"
_class
loc:@conv1d_3/kernel
đ
.conv1d_3/kernel/Initializer/random_uniform/mulMul8conv1d_3/kernel/Initializer/random_uniform/RandomUniform.conv1d_3/kernel/Initializer/random_uniform/sub*"
_output_shapes
:@*
T0*"
_class
loc:@conv1d_3/kernel
â
*conv1d_3/kernel/Initializer/random_uniformAdd.conv1d_3/kernel/Initializer/random_uniform/mul.conv1d_3/kernel/Initializer/random_uniform/min*
T0*"
_class
loc:@conv1d_3/kernel*"
_output_shapes
:@
ł
conv1d_3/kernelVarHandleOp*"
_class
loc:@conv1d_3/kernel*
	container *
shape:@*
dtype0*
_output_shapes
: * 
shared_nameconv1d_3/kernel
o
0conv1d_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_3/kernel*
_output_shapes
: 

conv1d_3/kernel/AssignAssignVariableOpconv1d_3/kernel*conv1d_3/kernel/Initializer/random_uniform*"
_class
loc:@conv1d_3/kernel*
dtype0

#conv1d_3/kernel/Read/ReadVariableOpReadVariableOpconv1d_3/kernel*
dtype0*"
_output_shapes
:@*"
_class
loc:@conv1d_3/kernel

conv1d_3/bias/Initializer/zerosConst*
dtype0*
_output_shapes
:@*
valueB@*    * 
_class
loc:@conv1d_3/bias
Ľ
conv1d_3/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_nameconv1d_3/bias* 
_class
loc:@conv1d_3/bias*
	container *
shape:@
k
.conv1d_3/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_3/bias*
_output_shapes
: 

conv1d_3/bias/AssignAssignVariableOpconv1d_3/biasconv1d_3/bias/Initializer/zeros* 
_class
loc:@conv1d_3/bias*
dtype0

!conv1d_3/bias/Read/ReadVariableOpReadVariableOpconv1d_3/bias* 
_class
loc:@conv1d_3/bias*
dtype0*
_output_shapes
:@
`
conv1d_3/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB:
`
conv1d_3/conv1d/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 

conv1d_3/conv1d/ExpandDims
ExpandDimsconv1d_3_inputconv1d_3/conv1d/ExpandDims/dim*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ú*

Tdim0*
T0

+conv1d_3/conv1d/ExpandDims_1/ReadVariableOpReadVariableOpconv1d_3/kernel*
dtype0*"
_output_shapes
:@
b
 conv1d_3/conv1d/ExpandDims_1/dimConst*
dtype0*
_output_shapes
: *
value	B : 
ś
conv1d_3/conv1d/ExpandDims_1
ExpandDims+conv1d_3/conv1d/ExpandDims_1/ReadVariableOp conv1d_3/conv1d/ExpandDims_1/dim*

Tdim0*
T0*&
_output_shapes
:@

conv1d_3/conv1d/Conv2DConv2Dconv1d_3/conv1d/ExpandDimsconv1d_3/conv1d/ExpandDims_1*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*
	dilations
*
T0

conv1d_3/conv1d/SqueezeSqueezeconv1d_3/conv1d/Conv2D*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*
squeeze_dims
*
T0
i
conv1d_3/BiasAdd/ReadVariableOpReadVariableOpconv1d_3/bias*
dtype0*
_output_shapes
:@
Ł
conv1d_3/BiasAddBiasAddconv1d_3/conv1d/Squeezeconv1d_3/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@
^
conv1d_3/ReluReluconv1d_3/BiasAdd*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*
T0
d
dropout_4/IdentityIdentityconv1d_3/Relu*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*
T0
Š
0conv1d_4/kernel/Initializer/random_uniform/shapeConst*!
valueB"   @   @   *"
_class
loc:@conv1d_4/kernel*
dtype0*
_output_shapes
:

.conv1d_4/kernel/Initializer/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *   ž*"
_class
loc:@conv1d_4/kernel

.conv1d_4/kernel/Initializer/random_uniform/maxConst*
valueB
 *   >*"
_class
loc:@conv1d_4/kernel*
dtype0*
_output_shapes
: 
ň
8conv1d_4/kernel/Initializer/random_uniform/RandomUniformRandomUniform0conv1d_4/kernel/Initializer/random_uniform/shape*
T0*"
_class
loc:@conv1d_4/kernel*
seed2 *
dtype0*"
_output_shapes
:@@*

seed 
Ú
.conv1d_4/kernel/Initializer/random_uniform/subSub.conv1d_4/kernel/Initializer/random_uniform/max.conv1d_4/kernel/Initializer/random_uniform/min*
T0*"
_class
loc:@conv1d_4/kernel*
_output_shapes
: 
đ
.conv1d_4/kernel/Initializer/random_uniform/mulMul8conv1d_4/kernel/Initializer/random_uniform/RandomUniform.conv1d_4/kernel/Initializer/random_uniform/sub*
T0*"
_class
loc:@conv1d_4/kernel*"
_output_shapes
:@@
â
*conv1d_4/kernel/Initializer/random_uniformAdd.conv1d_4/kernel/Initializer/random_uniform/mul.conv1d_4/kernel/Initializer/random_uniform/min*
T0*"
_class
loc:@conv1d_4/kernel*"
_output_shapes
:@@
ł
conv1d_4/kernelVarHandleOp*
	container *
shape:@@*
dtype0*
_output_shapes
: * 
shared_nameconv1d_4/kernel*"
_class
loc:@conv1d_4/kernel
o
0conv1d_4/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_4/kernel*
_output_shapes
: 

conv1d_4/kernel/AssignAssignVariableOpconv1d_4/kernel*conv1d_4/kernel/Initializer/random_uniform*"
_class
loc:@conv1d_4/kernel*
dtype0

#conv1d_4/kernel/Read/ReadVariableOpReadVariableOpconv1d_4/kernel*
dtype0*"
_output_shapes
:@@*"
_class
loc:@conv1d_4/kernel

conv1d_4/bias/Initializer/zerosConst*
valueB@*    * 
_class
loc:@conv1d_4/bias*
dtype0*
_output_shapes
:@
Ľ
conv1d_4/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_nameconv1d_4/bias* 
_class
loc:@conv1d_4/bias*
	container *
shape:@
k
.conv1d_4/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_4/bias*
_output_shapes
: 

conv1d_4/bias/AssignAssignVariableOpconv1d_4/biasconv1d_4/bias/Initializer/zeros* 
_class
loc:@conv1d_4/bias*
dtype0

!conv1d_4/bias/Read/ReadVariableOpReadVariableOpconv1d_4/bias* 
_class
loc:@conv1d_4/bias*
dtype0*
_output_shapes
:@
`
conv1d_4/dilation_rateConst*
valueB:*
dtype0*
_output_shapes
:
`
conv1d_4/conv1d/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
Ł
conv1d_4/conv1d/ExpandDims
ExpandDimsdropout_4/Identityconv1d_4/conv1d/ExpandDims/dim*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*

Tdim0*
T0

+conv1d_4/conv1d/ExpandDims_1/ReadVariableOpReadVariableOpconv1d_4/kernel*
dtype0*"
_output_shapes
:@@
b
 conv1d_4/conv1d/ExpandDims_1/dimConst*
dtype0*
_output_shapes
: *
value	B : 
ś
conv1d_4/conv1d/ExpandDims_1
ExpandDims+conv1d_4/conv1d/ExpandDims_1/ReadVariableOp conv1d_4/conv1d/ExpandDims_1/dim*&
_output_shapes
:@@*

Tdim0*
T0

conv1d_4/conv1d/Conv2DConv2Dconv1d_4/conv1d/ExpandDimsconv1d_4/conv1d/ExpandDims_1*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*
	dilations


conv1d_4/conv1d/SqueezeSqueezeconv1d_4/conv1d/Conv2D*
T0*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*
squeeze_dims

i
conv1d_4/BiasAdd/ReadVariableOpReadVariableOpconv1d_4/bias*
dtype0*
_output_shapes
:@
˘
conv1d_4/BiasAddBiasAddconv1d_4/conv1d/Squeezeconv1d_4/BiasAdd/ReadVariableOp*
data_formatNHWC*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*
T0
]
conv1d_4/ReluReluconv1d_4/BiasAdd*
T0*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@
c
dropout_5/IdentityIdentityconv1d_4/Relu*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*
T0
Š
0conv1d_5/kernel/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*!
valueB"   @       *"
_class
loc:@conv1d_5/kernel

.conv1d_5/kernel/Initializer/random_uniform/minConst*
valueB
 *:Íž*"
_class
loc:@conv1d_5/kernel*
dtype0*
_output_shapes
: 

.conv1d_5/kernel/Initializer/random_uniform/maxConst*
valueB
 *:Í>*"
_class
loc:@conv1d_5/kernel*
dtype0*
_output_shapes
: 
ň
8conv1d_5/kernel/Initializer/random_uniform/RandomUniformRandomUniform0conv1d_5/kernel/Initializer/random_uniform/shape*
seed2 *
dtype0*"
_output_shapes
:@ *

seed *
T0*"
_class
loc:@conv1d_5/kernel
Ú
.conv1d_5/kernel/Initializer/random_uniform/subSub.conv1d_5/kernel/Initializer/random_uniform/max.conv1d_5/kernel/Initializer/random_uniform/min*
T0*"
_class
loc:@conv1d_5/kernel*
_output_shapes
: 
đ
.conv1d_5/kernel/Initializer/random_uniform/mulMul8conv1d_5/kernel/Initializer/random_uniform/RandomUniform.conv1d_5/kernel/Initializer/random_uniform/sub*
T0*"
_class
loc:@conv1d_5/kernel*"
_output_shapes
:@ 
â
*conv1d_5/kernel/Initializer/random_uniformAdd.conv1d_5/kernel/Initializer/random_uniform/mul.conv1d_5/kernel/Initializer/random_uniform/min*
T0*"
_class
loc:@conv1d_5/kernel*"
_output_shapes
:@ 
ł
conv1d_5/kernelVarHandleOp*
dtype0*
_output_shapes
: * 
shared_nameconv1d_5/kernel*"
_class
loc:@conv1d_5/kernel*
	container *
shape:@ 
o
0conv1d_5/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_5/kernel*
_output_shapes
: 

conv1d_5/kernel/AssignAssignVariableOpconv1d_5/kernel*conv1d_5/kernel/Initializer/random_uniform*"
_class
loc:@conv1d_5/kernel*
dtype0

#conv1d_5/kernel/Read/ReadVariableOpReadVariableOpconv1d_5/kernel*"
_class
loc:@conv1d_5/kernel*
dtype0*"
_output_shapes
:@ 

conv1d_5/bias/Initializer/zerosConst*
dtype0*
_output_shapes
: *
valueB *    * 
_class
loc:@conv1d_5/bias
Ľ
conv1d_5/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_nameconv1d_5/bias* 
_class
loc:@conv1d_5/bias*
	container *
shape: 
k
.conv1d_5/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpconv1d_5/bias*
_output_shapes
: 

conv1d_5/bias/AssignAssignVariableOpconv1d_5/biasconv1d_5/bias/Initializer/zeros* 
_class
loc:@conv1d_5/bias*
dtype0

!conv1d_5/bias/Read/ReadVariableOpReadVariableOpconv1d_5/bias* 
_class
loc:@conv1d_5/bias*
dtype0*
_output_shapes
: 
`
conv1d_5/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB:
`
conv1d_5/conv1d/ExpandDims/dimConst*
value	B :*
dtype0*
_output_shapes
: 
˘
conv1d_5/conv1d/ExpandDims
ExpandDimsdropout_5/Identityconv1d_5/conv1d/ExpandDims/dim*
T0*/
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*

Tdim0

+conv1d_5/conv1d/ExpandDims_1/ReadVariableOpReadVariableOpconv1d_5/kernel*
dtype0*"
_output_shapes
:@ 
b
 conv1d_5/conv1d/ExpandDims_1/dimConst*
value	B : *
dtype0*
_output_shapes
: 
ś
conv1d_5/conv1d/ExpandDims_1
ExpandDims+conv1d_5/conv1d/ExpandDims_1/ReadVariableOp conv1d_5/conv1d/ExpandDims_1/dim*&
_output_shapes
:@ *

Tdim0*
T0

conv1d_5/conv1d/Conv2DConv2Dconv1d_5/conv1d/ExpandDimsconv1d_5/conv1d/ExpandDims_1*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙ *
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(

conv1d_5/conv1d/SqueezeSqueezeconv1d_5/conv1d/Conv2D*
T0*+
_output_shapes
:˙˙˙˙˙˙˙˙˙ *
squeeze_dims

i
conv1d_5/BiasAdd/ReadVariableOpReadVariableOpconv1d_5/bias*
dtype0*
_output_shapes
: 
˘
conv1d_5/BiasAddBiasAddconv1d_5/conv1d/Squeezeconv1d_5/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*+
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
]
conv1d_5/ReluReluconv1d_5/BiasAdd*
T0*+
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
\
flatten_1/ShapeShapeconv1d_5/Relu*
T0*
out_type0*
_output_shapes
:
g
flatten_1/strided_slice/stackConst*
valueB: *
dtype0*
_output_shapes
:
i
flatten_1/strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
i
flatten_1/strided_slice/stack_2Const*
dtype0*
_output_shapes
:*
valueB:
Ť
flatten_1/strided_sliceStridedSliceflatten_1/Shapeflatten_1/strided_slice/stackflatten_1/strided_slice/stack_1flatten_1/strided_slice/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
d
flatten_1/Reshape/shape/1Const*
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
: 

flatten_1/Reshape/shapePackflatten_1/strided_sliceflatten_1/Reshape/shape/1*
T0*

axis *
N*
_output_shapes
:

flatten_1/ReshapeReshapeconv1d_5/Reluflatten_1/Reshape/shape*
T0*
Tshape0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙ŕ
d
dropout_6/IdentityIdentityflatten_1/Reshape*(
_output_shapes
:˙˙˙˙˙˙˙˙˙ŕ*
T0
Ł
/dense_2/kernel/Initializer/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"`      *!
_class
loc:@dense_2/kernel

-dense_2/kernel/Initializer/random_uniform/minConst*
valueB
 *b§˝*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 

-dense_2/kernel/Initializer/random_uniform/maxConst*
valueB
 *b§=*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
: 
ě
7dense_2/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_2/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes
:	ŕ *

seed *
T0*!
_class
loc:@dense_2/kernel*
seed2 
Ö
-dense_2/kernel/Initializer/random_uniform/subSub-dense_2/kernel/Initializer/random_uniform/max-dense_2/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes
: 
é
-dense_2/kernel/Initializer/random_uniform/mulMul7dense_2/kernel/Initializer/random_uniform/RandomUniform-dense_2/kernel/Initializer/random_uniform/sub*
T0*!
_class
loc:@dense_2/kernel*
_output_shapes
:	ŕ 
Ű
)dense_2/kernel/Initializer/random_uniformAdd-dense_2/kernel/Initializer/random_uniform/mul-dense_2/kernel/Initializer/random_uniform/min*
_output_shapes
:	ŕ *
T0*!
_class
loc:@dense_2/kernel
­
dense_2/kernelVarHandleOp*!
_class
loc:@dense_2/kernel*
	container *
shape:	ŕ *
dtype0*
_output_shapes
: *
shared_namedense_2/kernel
m
/dense_2/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/kernel*
_output_shapes
: 

dense_2/kernel/AssignAssignVariableOpdense_2/kernel)dense_2/kernel/Initializer/random_uniform*!
_class
loc:@dense_2/kernel*
dtype0

"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes
:	ŕ 

dense_2/bias/Initializer/zerosConst*
valueB *    *
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
˘
dense_2/biasVarHandleOp*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_namedense_2/bias*
_class
loc:@dense_2/bias
i
-dense_2/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/bias*
_output_shapes
: 

dense_2/bias/AssignAssignVariableOpdense_2/biasdense_2/bias/Initializer/zeros*
dtype0*
_class
loc:@dense_2/bias

 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
: 
m
dense_2/MatMul/ReadVariableOpReadVariableOpdense_2/kernel*
dtype0*
_output_shapes
:	ŕ 
Ł
dense_2/MatMulMatMuldropout_6/Identitydense_2/MatMul/ReadVariableOp*
T0*
transpose_a( *'
_output_shapes
:˙˙˙˙˙˙˙˙˙ *
transpose_b( 
g
dense_2/BiasAdd/ReadVariableOpReadVariableOpdense_2/bias*
dtype0*
_output_shapes
: 

dense_2/BiasAddBiasAdddense_2/MatMuldense_2/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
]
dense_2/SigmoidSigmoiddense_2/BiasAdd*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
a
dropout_7/IdentityIdentitydense_2/Sigmoid*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
Ł
/dense_3/kernel/Initializer/random_uniform/shapeConst*
valueB"       *!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
:

-dense_3/kernel/Initializer/random_uniform/minConst*
valueB
 *ťrËž*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 

-dense_3/kernel/Initializer/random_uniform/maxConst*
valueB
 *ťrË>*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes
: 
ë
7dense_3/kernel/Initializer/random_uniform/RandomUniformRandomUniform/dense_3/kernel/Initializer/random_uniform/shape*
dtype0*
_output_shapes

: *

seed *
T0*!
_class
loc:@dense_3/kernel*
seed2 
Ö
-dense_3/kernel/Initializer/random_uniform/subSub-dense_3/kernel/Initializer/random_uniform/max-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes
: 
č
-dense_3/kernel/Initializer/random_uniform/mulMul7dense_3/kernel/Initializer/random_uniform/RandomUniform-dense_3/kernel/Initializer/random_uniform/sub*
_output_shapes

: *
T0*!
_class
loc:@dense_3/kernel
Ú
)dense_3/kernel/Initializer/random_uniformAdd-dense_3/kernel/Initializer/random_uniform/mul-dense_3/kernel/Initializer/random_uniform/min*
T0*!
_class
loc:@dense_3/kernel*
_output_shapes

: 
Ź
dense_3/kernelVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_3/kernel*!
_class
loc:@dense_3/kernel*
	container *
shape
: 
m
/dense_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/kernel*
_output_shapes
: 

dense_3/kernel/AssignAssignVariableOpdense_3/kernel)dense_3/kernel/Initializer/random_uniform*
dtype0*!
_class
loc:@dense_3/kernel

"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes

: 

dense_3/bias/Initializer/zerosConst*
valueB*    *
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:
˘
dense_3/biasVarHandleOp*
shared_namedense_3/bias*
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_3/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/bias*
_output_shapes
: 

dense_3/bias/AssignAssignVariableOpdense_3/biasdense_3/bias/Initializer/zeros*
_class
loc:@dense_3/bias*
dtype0

 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
dtype0*
_output_shapes
:*
_class
loc:@dense_3/bias
l
dense_3/MatMul/ReadVariableOpReadVariableOpdense_3/kernel*
dtype0*
_output_shapes

: 
Ł
dense_3/MatMulMatMuldropout_7/Identitydense_3/MatMul/ReadVariableOp*
T0*
transpose_a( *'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
transpose_b( 
g
dense_3/BiasAdd/ReadVariableOpReadVariableOpdense_3/bias*
dtype0*
_output_shapes
:

dense_3/BiasAddBiasAdddense_3/MatMuldense_3/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
]
dense_3/SoftmaxSoftmaxdense_3/BiasAdd*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
d
PlaceholderPlaceholder*
dtype0*"
_output_shapes
:@*
shape:@
O
AssignVariableOpAssignVariableOpconv1d_3/kernelPlaceholder*
dtype0
u
ReadVariableOpReadVariableOpconv1d_3/kernel^AssignVariableOp*
dtype0*"
_output_shapes
:@
V
Placeholder_1Placeholder*
dtype0*
_output_shapes
:@*
shape:@
Q
AssignVariableOp_1AssignVariableOpconv1d_3/biasPlaceholder_1*
dtype0
o
ReadVariableOp_1ReadVariableOpconv1d_3/bias^AssignVariableOp_1*
dtype0*
_output_shapes
:@
f
Placeholder_2Placeholder*
dtype0*"
_output_shapes
:@@*
shape:@@
S
AssignVariableOp_2AssignVariableOpconv1d_4/kernelPlaceholder_2*
dtype0
y
ReadVariableOp_2ReadVariableOpconv1d_4/kernel^AssignVariableOp_2*
dtype0*"
_output_shapes
:@@
V
Placeholder_3Placeholder*
shape:@*
dtype0*
_output_shapes
:@
Q
AssignVariableOp_3AssignVariableOpconv1d_4/biasPlaceholder_3*
dtype0
o
ReadVariableOp_3ReadVariableOpconv1d_4/bias^AssignVariableOp_3*
dtype0*
_output_shapes
:@
f
Placeholder_4Placeholder*
dtype0*"
_output_shapes
:@ *
shape:@ 
S
AssignVariableOp_4AssignVariableOpconv1d_5/kernelPlaceholder_4*
dtype0
y
ReadVariableOp_4ReadVariableOpconv1d_5/kernel^AssignVariableOp_4*
dtype0*"
_output_shapes
:@ 
V
Placeholder_5Placeholder*
shape: *
dtype0*
_output_shapes
: 
Q
AssignVariableOp_5AssignVariableOpconv1d_5/biasPlaceholder_5*
dtype0
o
ReadVariableOp_5ReadVariableOpconv1d_5/bias^AssignVariableOp_5*
dtype0*
_output_shapes
: 
`
Placeholder_6Placeholder*
dtype0*
_output_shapes
:	ŕ *
shape:	ŕ 
R
AssignVariableOp_6AssignVariableOpdense_2/kernelPlaceholder_6*
dtype0
u
ReadVariableOp_6ReadVariableOpdense_2/kernel^AssignVariableOp_6*
dtype0*
_output_shapes
:	ŕ 
V
Placeholder_7Placeholder*
dtype0*
_output_shapes
: *
shape: 
P
AssignVariableOp_7AssignVariableOpdense_2/biasPlaceholder_7*
dtype0
n
ReadVariableOp_7ReadVariableOpdense_2/bias^AssignVariableOp_7*
dtype0*
_output_shapes
: 
^
Placeholder_8Placeholder*
shape
: *
dtype0*
_output_shapes

: 
R
AssignVariableOp_8AssignVariableOpdense_3/kernelPlaceholder_8*
dtype0
t
ReadVariableOp_8ReadVariableOpdense_3/kernel^AssignVariableOp_8*
dtype0*
_output_shapes

: 
V
Placeholder_9Placeholder*
dtype0*
_output_shapes
:*
shape:
P
AssignVariableOp_9AssignVariableOpdense_3/biasPlaceholder_9*
dtype0
n
ReadVariableOp_9ReadVariableOpdense_3/bias^AssignVariableOp_9*
dtype0*
_output_shapes
:
O
VarIsInitializedOpVarIsInitializedOpconv1d_4/bias*
_output_shapes
: 
P
VarIsInitializedOp_1VarIsInitializedOpdense_3/bias*
_output_shapes
: 
S
VarIsInitializedOp_2VarIsInitializedOpconv1d_3/kernel*
_output_shapes
: 
S
VarIsInitializedOp_3VarIsInitializedOpconv1d_4/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_4VarIsInitializedOpconv1d_3/bias*
_output_shapes
: 
R
VarIsInitializedOp_5VarIsInitializedOpdense_3/kernel*
_output_shapes
: 
R
VarIsInitializedOp_6VarIsInitializedOpdense_2/kernel*
_output_shapes
: 
P
VarIsInitializedOp_7VarIsInitializedOpdense_2/bias*
_output_shapes
: 
S
VarIsInitializedOp_8VarIsInitializedOpconv1d_5/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_9VarIsInitializedOpconv1d_5/bias*
_output_shapes
: 
ř
initNoOp^conv1d_3/bias/Assign^conv1d_3/kernel/Assign^conv1d_4/bias/Assign^conv1d_4/kernel/Assign^conv1d_5/bias/Assign^conv1d_5/kernel/Assign^dense_2/bias/Assign^dense_2/kernel/Assign^dense_3/bias/Assign^dense_3/kernel/Assign

$RMSprop/lr/Initializer/initial_valueConst*
valueB
 *o:*
_class
loc:@RMSprop/lr*
dtype0*
_output_shapes
: 


RMSprop/lrVarHandleOp*
_class
loc:@RMSprop/lr*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_name
RMSprop/lr
e
+RMSprop/lr/IsInitialized/VarIsInitializedOpVarIsInitializedOp
RMSprop/lr*
_output_shapes
: 

RMSprop/lr/AssignAssignVariableOp
RMSprop/lr$RMSprop/lr/Initializer/initial_value*
dtype0*
_class
loc:@RMSprop/lr

RMSprop/lr/Read/ReadVariableOpReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: *
_class
loc:@RMSprop/lr

%RMSprop/rho/Initializer/initial_valueConst*
valueB
 *fff?*
_class
loc:@RMSprop/rho*
dtype0*
_output_shapes
: 

RMSprop/rhoVarHandleOp*
_class
loc:@RMSprop/rho*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameRMSprop/rho
g
,RMSprop/rho/IsInitialized/VarIsInitializedOpVarIsInitializedOpRMSprop/rho*
_output_shapes
: 

RMSprop/rho/AssignAssignVariableOpRMSprop/rho%RMSprop/rho/Initializer/initial_value*
_class
loc:@RMSprop/rho*
dtype0

RMSprop/rho/Read/ReadVariableOpReadVariableOpRMSprop/rho*
_class
loc:@RMSprop/rho*
dtype0*
_output_shapes
: 

'RMSprop/decay/Initializer/initial_valueConst*
valueB
 *    * 
_class
loc:@RMSprop/decay*
dtype0*
_output_shapes
: 
Ą
RMSprop/decayVarHandleOp*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameRMSprop/decay* 
_class
loc:@RMSprop/decay
k
.RMSprop/decay/IsInitialized/VarIsInitializedOpVarIsInitializedOpRMSprop/decay*
_output_shapes
: 

RMSprop/decay/AssignAssignVariableOpRMSprop/decay'RMSprop/decay/Initializer/initial_value* 
_class
loc:@RMSprop/decay*
dtype0

!RMSprop/decay/Read/ReadVariableOpReadVariableOpRMSprop/decay* 
_class
loc:@RMSprop/decay*
dtype0*
_output_shapes
: 

,RMSprop/iterations/Initializer/initial_valueConst*
value	B	 R *%
_class
loc:@RMSprop/iterations*
dtype0	*
_output_shapes
: 
°
RMSprop/iterationsVarHandleOp*
	container *
shape: *
dtype0	*
_output_shapes
: *#
shared_nameRMSprop/iterations*%
_class
loc:@RMSprop/iterations
u
3RMSprop/iterations/IsInitialized/VarIsInitializedOpVarIsInitializedOpRMSprop/iterations*
_output_shapes
: 
Ł
RMSprop/iterations/AssignAssignVariableOpRMSprop/iterations,RMSprop/iterations/Initializer/initial_value*%
_class
loc:@RMSprop/iterations*
dtype0	

&RMSprop/iterations/Read/ReadVariableOpReadVariableOpRMSprop/iterations*%
_class
loc:@RMSprop/iterations*
dtype0	*
_output_shapes
: 

dense_3_targetPlaceholder*
dtype0*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*%
shape:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
R
ConstConst*
valueB*  ?*
dtype0*
_output_shapes
:

dense_3_sample_weightsPlaceholderWithDefaultConst*
shape:˙˙˙˙˙˙˙˙˙*
dtype0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
i
'loss/dense_3_loss/Sum/reduction_indicesConst*
value	B :*
dtype0*
_output_shapes
: 
Ľ
loss/dense_3_loss/SumSumdense_3/Softmax'loss/dense_3_loss/Sum/reduction_indices*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0*
	keep_dims(*
T0
~
loss/dense_3_loss/truedivRealDivdense_3/Softmaxloss/dense_3_loss/Sum*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
\
loss/dense_3_loss/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *żÖ3
\
loss/dense_3_loss/sub/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
o
loss/dense_3_loss/subSubloss/dense_3_loss/sub/xloss/dense_3_loss/Const*
T0*
_output_shapes
: 

'loss/dense_3_loss/clip_by_value/MinimumMinimumloss/dense_3_loss/truedivloss/dense_3_loss/sub*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

loss/dense_3_loss/clip_by_valueMaximum'loss/dense_3_loss/clip_by_value/Minimumloss/dense_3_loss/Const*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
o
loss/dense_3_loss/LogLogloss/dense_3_loss/clip_by_value*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
u
loss/dense_3_loss/mulMuldense_3_targetloss/dense_3_loss/Log*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
k
)loss/dense_3_loss/Sum_1/reduction_indicesConst*
value	B :*
dtype0*
_output_shapes
: 
Ť
loss/dense_3_loss/Sum_1Sumloss/dense_3_loss/mul)loss/dense_3_loss/Sum_1/reduction_indices*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0*
	keep_dims( *
T0
c
loss/dense_3_loss/NegNegloss/dense_3_loss/Sum_1*
T0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙

Floss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shapeShapedense_3_sample_weights*
T0*
out_type0*
_output_shapes
:

Eloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rankConst*
value	B :*
dtype0*
_output_shapes
: 

Eloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shapeShapeloss/dense_3_loss/Neg*
T0*
out_type0*
_output_shapes
:

Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/rankConst*
value	B :*
dtype0*
_output_shapes
: 

Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar/xConst*
dtype0*
_output_shapes
: *
value	B : 
ů
Bloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalarEqualDloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar/xEloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rank*
_output_shapes
: *
T0

Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/SwitchSwitchBloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalarBloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar*
T0
*
_output_shapes
: : 
Ď
Ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_tIdentityPloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch:1*
T0
*
_output_shapes
: 
Í
Ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_fIdentityNloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch*
_output_shapes
: *
T0

Ŕ
Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_idIdentityBloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar*
T0
*
_output_shapes
: 
é
Ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch_1SwitchBloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalarOloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id*
T0
*U
_classK
IGloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar*
_output_shapes
: : 

nloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rankEqualuloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switchwloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch_1*
T0*
_output_shapes
: 

uloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/SwitchSwitchDloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/rankOloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id*
T0*W
_classM
KIloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/values/rank*
_output_shapes
: : 

wloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch_1SwitchEloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rankOloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id*
_output_shapes
: : *
T0*X
_classN
LJloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rank
ő
hloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/SwitchSwitchnloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_ranknloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank*
T0
*
_output_shapes
: : 

jloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_tIdentityjloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch:1*
_output_shapes
: *
T0


jloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_fIdentityhloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch*
T0
*
_output_shapes
: 

iloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_idIdentitynloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank*
T0
*
_output_shapes
: 
ş
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/dimConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
: 
Ď
}loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims
ExpandDimsloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch_1:1loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/dim*
T0*
_output_shapes

:*

Tdim0
Ź
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/SwitchSwitchEloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shapeOloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id*
T0*X
_classN
LJloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape* 
_output_shapes
::

loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch_1Switchloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switchiloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id*
T0*X
_classN
LJloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape* 
_output_shapes
::
Á
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/ShapeConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
dtype0*
_output_shapes
:*
valueB"      
˛
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/ConstConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
value	B :*
dtype0*
_output_shapes
: 
É
|loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_likeFillloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Shapeloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Const*
T0*

index_type0*
_output_shapes

:
­
~loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat/axisConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
value	B :*
dtype0*
_output_shapes
: 
Ŕ
yloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concatConcatV2}loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims|loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like~loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat/axis*

Tidx0*
T0*
N*
_output_shapes

:
ź
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/dimConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
: 
Ő
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1
ExpandDimsloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch_1:1loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/dim*
_output_shapes

:*

Tdim0*
T0
°
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/SwitchSwitchFloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shapeOloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id*
T0*Y
_classO
MKloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape* 
_output_shapes
::

loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch_1Switchloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switchiloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id* 
_output_shapes
::*
T0*Y
_classO
MKloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape

loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperationDenseToDenseSetOperationloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1yloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat*<
_output_shapes*
(:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙:*
set_operationa-b*
T0*
validate_indices(
Í
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/num_invalid_dimsSizeloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:1*
T0*
out_type0*
_output_shapes
: 
Ł
tloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/xConstk^loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t*
dtype0*
_output_shapes
: *
value	B : 

rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dimsEqualtloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/xloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/num_invalid_dims*
T0*
_output_shapes
: 
ö
jloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1Switchnloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rankiloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id*
T0
*
_classw
usloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank*
_output_shapes
: : 
ü
gloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/MergeMergejloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims*
T0
*
N*
_output_shapes
: : 
ż
Mloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/MergeMergegloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/MergeRloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch_1:1*
T0
*
N*
_output_shapes
: : 
Ś
>loss/dense_3_loss/broadcast_weights/assert_broadcastable/ConstConst*8
value/B- B'weights can not be broadcast to values.*
dtype0*
_output_shapes
: 

@loss/dense_3_loss/broadcast_weights/assert_broadcastable/Const_1Const*
dtype0*
_output_shapes
: *
valueB Bweights.shape=

@loss/dense_3_loss/broadcast_weights/assert_broadcastable/Const_2Const*)
value B Bdense_3_sample_weights:0*
dtype0*
_output_shapes
: 

@loss/dense_3_loss/broadcast_weights/assert_broadcastable/Const_3Const*
valueB Bvalues.shape=*
dtype0*
_output_shapes
: 

@loss/dense_3_loss/broadcast_weights/assert_broadcastable/Const_4Const*(
valueB Bloss/dense_3_loss/Neg:0*
dtype0*
_output_shapes
: 

@loss/dense_3_loss/broadcast_weights/assert_broadcastable/Const_5Const*
valueB B
is_scalar=*
dtype0*
_output_shapes
: 

Kloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/SwitchSwitchMloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/MergeMloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Merge*
T0
*
_output_shapes
: : 
É
Mloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_tIdentityMloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Switch:1*
_output_shapes
: *
T0

Ç
Mloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_fIdentityKloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Switch*
_output_shapes
: *
T0

Č
Lloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_idIdentityMloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Merge*
T0
*
_output_shapes
: 
Ą
Iloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/NoOpNoOpN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_t

Wloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependencyIdentityMloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_tJ^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/NoOp*
_output_shapes
: *
T0
*`
_classV
TRloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_t

Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_0ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*
dtype0*
_output_shapes
: *8
value/B- B'weights can not be broadcast to values.
ń
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_1ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*
dtype0*
_output_shapes
: *
valueB Bweights.shape=
ű
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_2ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*
dtype0*
_output_shapes
: *)
value B Bdense_3_sample_weights:0
đ
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_4ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*
valueB Bvalues.shape=*
dtype0*
_output_shapes
: 
ú
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_5ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*(
valueB Bloss/dense_3_loss/Neg:0*
dtype0*
_output_shapes
: 
í
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_7ConstN^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f*
valueB B
is_scalar=*
dtype0*
_output_shapes
: 
Č
Kloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/AssertAssertRloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/SwitchRloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_0Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_1Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_2Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_1Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_4Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_5Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_2Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_7Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_3*
T
2	
*
	summarize
ţ
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/SwitchSwitchMloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/MergeLloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id*
_output_shapes
: : *
T0
*`
_classV
TRloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Merge
ú
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_1SwitchFloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shapeLloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id*
T0*Y
_classO
MKloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape* 
_output_shapes
::
ř
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_2SwitchEloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shapeLloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id*
T0*X
_classN
LJloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape* 
_output_shapes
::
ę
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_3SwitchBloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalarLloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id*
T0
*U
_classK
IGloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar*
_output_shapes
: : 

Yloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependency_1IdentityMloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_fL^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert*
_output_shapes
: *
T0
*`
_classV
TRloc:@loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f
ł
Jloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/MergeMergeYloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependency_1Wloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependency*
T0
*
N*
_output_shapes
: : 
Ő
3loss/dense_3_loss/broadcast_weights/ones_like/ShapeShapeloss/dense_3_loss/NegK^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Merge*
_output_shapes
:*
T0*
out_type0
Ĺ
3loss/dense_3_loss/broadcast_weights/ones_like/ConstConstK^loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Merge*
valueB
 *  ?*
dtype0*
_output_shapes
: 
ß
-loss/dense_3_loss/broadcast_weights/ones_likeFill3loss/dense_3_loss/broadcast_weights/ones_like/Shape3loss/dense_3_loss/broadcast_weights/ones_like/Const*
T0*

index_type0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙

#loss/dense_3_loss/broadcast_weightsMuldense_3_sample_weights-loss/dense_3_loss/broadcast_weights/ones_like*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

loss/dense_3_loss/Mul_1Mulloss/dense_3_loss/Neg#loss/dense_3_loss/broadcast_weights*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0
c
loss/dense_3_loss/Const_1Const*
valueB: *
dtype0*
_output_shapes
:

loss/dense_3_loss/Sum_2Sumloss/dense_3_loss/Mul_1loss/dense_3_loss/Const_1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
c
loss/dense_3_loss/Const_2Const*
dtype0*
_output_shapes
:*
valueB: 

loss/dense_3_loss/Sum_3Sum#loss/dense_3_loss/broadcast_weightsloss/dense_3_loss/Const_2*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
y
loss/dense_3_loss/truediv_1RealDivloss/dense_3_loss/Sum_2loss/dense_3_loss/Sum_3*
T0*
_output_shapes
: 
a
loss/dense_3_loss/zeros_likeConst*
dtype0*
_output_shapes
: *
valueB
 *    
|
loss/dense_3_loss/GreaterGreaterloss/dense_3_loss/Sum_3loss/dense_3_loss/zeros_like*
T0*
_output_shapes
: 

loss/dense_3_loss/SelectSelectloss/dense_3_loss/Greaterloss/dense_3_loss/truediv_1loss/dense_3_loss/zeros_like*
T0*
_output_shapes
: 
\
loss/dense_3_loss/Const_3Const*
valueB *
dtype0*
_output_shapes
: 

loss/dense_3_loss/MeanMeanloss/dense_3_loss/Selectloss/dense_3_loss/Const_3*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
O

loss/mul/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
T
loss/mulMul
loss/mul/xloss/dense_3_loss/Mean*
T0*
_output_shapes
: 
g
metrics/acc/ArgMax/dimensionConst*
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
: 

metrics/acc/ArgMaxArgMaxdense_3_targetmetrics/acc/ArgMax/dimension*
T0*
output_type0	*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0
i
metrics/acc/ArgMax_1/dimensionConst*
valueB :
˙˙˙˙˙˙˙˙˙*
dtype0*
_output_shapes
: 

metrics/acc/ArgMax_1ArgMaxdense_3/Softmaxmetrics/acc/ArgMax_1/dimension*
output_type0	*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0*
T0
r
metrics/acc/EqualEqualmetrics/acc/ArgMaxmetrics/acc/ArgMax_1*
T0	*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
x
metrics/acc/CastCastmetrics/acc/Equal*

SrcT0
*
Truncate( *

DstT0*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
[
metrics/acc/ConstConst*
valueB: *
dtype0*
_output_shapes
:
{
metrics/acc/MeanMeanmetrics/acc/Castmetrics/acc/Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0

 training/RMSprop/gradients/ShapeConst*
valueB *
_class
loc:@loss/mul*
dtype0*
_output_shapes
: 

$training/RMSprop/gradients/grad_ys_0Const*
valueB
 *  ?*
_class
loc:@loss/mul*
dtype0*
_output_shapes
: 
ż
training/RMSprop/gradients/FillFill training/RMSprop/gradients/Shape$training/RMSprop/gradients/grad_ys_0*
T0*

index_type0*
_class
loc:@loss/mul*
_output_shapes
: 
Ş
,training/RMSprop/gradients/loss/mul_grad/MulMultraining/RMSprop/gradients/Fillloss/dense_3_loss/Mean*
T0*
_class
loc:@loss/mul*
_output_shapes
: 
 
.training/RMSprop/gradients/loss/mul_grad/Mul_1Multraining/RMSprop/gradients/Fill
loss/mul/x*
_output_shapes
: *
T0*
_class
loc:@loss/mul
˛
Dtraining/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Reshape/shapeConst*
valueB *)
_class
loc:@loss/dense_3_loss/Mean*
dtype0*
_output_shapes
: 

>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/ReshapeReshape.training/RMSprop/gradients/loss/mul_grad/Mul_1Dtraining/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Reshape/shape*
T0*
Tshape0*)
_class
loc:@loss/dense_3_loss/Mean*
_output_shapes
: 
Ş
<training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/ConstConst*
valueB *)
_class
loc:@loss/dense_3_loss/Mean*
dtype0*
_output_shapes
: 

;training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/TileTile>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Reshape<training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Const*
_output_shapes
: *

Tmultiples0*
T0*)
_class
loc:@loss/dense_3_loss/Mean
Ž
>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Const_1Const*
valueB
 *  ?*)
_class
loc:@loss/dense_3_loss/Mean*
dtype0*
_output_shapes
: 

>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/truedivRealDiv;training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Tile>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/Const_1*
T0*)
_class
loc:@loss/dense_3_loss/Mean*
_output_shapes
: 
ľ
Ctraining/RMSprop/gradients/loss/dense_3_loss/Select_grad/zeros_likeConst*
valueB
 *    *+
_class!
loc:@loss/dense_3_loss/Select*
dtype0*
_output_shapes
: 
ˇ
?training/RMSprop/gradients/loss/dense_3_loss/Select_grad/SelectSelectloss/dense_3_loss/Greater>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/truedivCtraining/RMSprop/gradients/loss/dense_3_loss/Select_grad/zeros_like*
T0*+
_class!
loc:@loss/dense_3_loss/Select*
_output_shapes
: 
š
Atraining/RMSprop/gradients/loss/dense_3_loss/Select_grad/Select_1Selectloss/dense_3_loss/GreaterCtraining/RMSprop/gradients/loss/dense_3_loss/Select_grad/zeros_like>training/RMSprop/gradients/loss/dense_3_loss/Mean_grad/truediv*
_output_shapes
: *
T0*+
_class!
loc:@loss/dense_3_loss/Select
´
Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/ShapeConst*
valueB *.
_class$
" loc:@loss/dense_3_loss/truediv_1*
dtype0*
_output_shapes
: 
ś
Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB *.
_class$
" loc:@loss/dense_3_loss/truediv_1
ß
Qtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/BroadcastGradientArgsBroadcastGradientArgsAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/ShapeCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1
ů
Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDivRealDiv?training/RMSprop/gradients/loss/dense_3_loss/Select_grad/Selectloss/dense_3_loss/Sum_3*
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
Ě
?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/SumSumCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDivQtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
ą
Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/ReshapeReshape?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/SumAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Shape*
T0*
Tshape0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
°
?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/NegNegloss/dense_3_loss/Sum_2*
_output_shapes
: *
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1
ű
Etraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDiv_1RealDiv?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Negloss/dense_3_loss/Sum_3*
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 

Etraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDiv_2RealDivEtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDiv_1loss/dense_3_loss/Sum_3*
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 

?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/mulMul?training/RMSprop/gradients/loss/dense_3_loss/Select_grad/SelectEtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/RealDiv_2*
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
Ě
Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Sum_1Sum?training/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/mulStraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
ˇ
Etraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Reshape_1ReshapeAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Sum_1Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/Shape_1*
T0*
Tshape0*.
_class$
" loc:@loss/dense_3_loss/truediv_1*
_output_shapes
: 
ť
Etraining/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Reshape/shapeConst*
valueB:**
_class 
loc:@loss/dense_3_loss/Sum_2*
dtype0*
_output_shapes
:
ľ
?training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/ReshapeReshapeCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_1_grad/ReshapeEtraining/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Reshape/shape*
T0*
Tshape0**
_class 
loc:@loss/dense_3_loss/Sum_2*
_output_shapes
:
Ŕ
=training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/ShapeShapeloss/dense_3_loss/Mul_1*
_output_shapes
:*
T0*
out_type0**
_class 
loc:@loss/dense_3_loss/Sum_2
°
<training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/TileTile?training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Reshape=training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Shape*
T0**
_class 
loc:@loss/dense_3_loss/Sum_2*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tmultiples0
ž
=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/ShapeShapeloss/dense_3_loss/Neg*
_output_shapes
:*
T0*
out_type0**
_class 
loc:@loss/dense_3_loss/Mul_1
Î
?training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Shape_1Shape#loss/dense_3_loss/broadcast_weights*
T0*
out_type0**
_class 
loc:@loss/dense_3_loss/Mul_1*
_output_shapes
:
Ď
Mtraining/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Shape?training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Shape_1*
T0**
_class 
loc:@loss/dense_3_loss/Mul_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
˙
;training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/MulMul<training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Tile#loss/dense_3_loss/broadcast_weights*#
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0**
_class 
loc:@loss/dense_3_loss/Mul_1
ş
;training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/SumSum;training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/MulMtraining/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/BroadcastGradientArgs*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0**
_class 
loc:@loss/dense_3_loss/Mul_1
Ž
?training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/ReshapeReshape;training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Sum=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Shape*
T0*
Tshape0**
_class 
loc:@loss/dense_3_loss/Mul_1*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
ó
=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Mul_1Mulloss/dense_3_loss/Neg<training/RMSprop/gradients/loss/dense_3_loss/Sum_2_grad/Tile*
T0**
_class 
loc:@loss/dense_3_loss/Mul_1*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ŕ
=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Sum_1Sum=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Mul_1Otraining/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/BroadcastGradientArgs:1*
T0**
_class 
loc:@loss/dense_3_loss/Mul_1*
_output_shapes
:*

Tidx0*
	keep_dims( 
´
Atraining/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Reshape_1Reshape=training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Sum_1?training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Shape_1*
T0*
Tshape0**
_class 
loc:@loss/dense_3_loss/Mul_1*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ů
9training/RMSprop/gradients/loss/dense_3_loss/Neg_grad/NegNeg?training/RMSprop/gradients/loss/dense_3_loss/Mul_1_grad/Reshape*
T0*(
_class
loc:@loss/dense_3_loss/Neg*#
_output_shapes
:˙˙˙˙˙˙˙˙˙
ž
=training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/ShapeShapeloss/dense_3_loss/mul*
_output_shapes
:*
T0*
out_type0**
_class 
loc:@loss/dense_3_loss/Sum_1
Ş
<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/SizeConst*
value	B :**
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
ř
;training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/addAdd)loss/dense_3_loss/Sum_1/reduction_indices<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Size*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
: 

;training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/modFloorMod;training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/add<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Size*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
: 
Ž
?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Shape_1Const*
valueB **
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
ą
Ctraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/range/startConst*
value	B : **
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
ą
Ctraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/range/deltaConst*
value	B :**
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
â
=training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/rangeRangeCtraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/range/start<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/SizeCtraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/range/delta**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
:*

Tidx0
°
Btraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Fill/valueConst*
value	B :**
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
¨
<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/FillFill?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Shape_1Btraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Fill/value*
T0*

index_type0**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
: 
Š
Etraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/DynamicStitchDynamicStitch=training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/range;training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/mod=training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Shape<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Fill*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*
N*
_output_shapes
:
Ż
Atraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Maximum/yConst*
value	B :**
_class 
loc:@loss/dense_3_loss/Sum_1*
dtype0*
_output_shapes
: 
Ľ
?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/MaximumMaximumEtraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/DynamicStitchAtraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Maximum/y*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
:

@training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/floordivFloorDiv=training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Shape?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Maximum*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*
_output_shapes
:
Á
?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/ReshapeReshape9training/RMSprop/gradients/loss/dense_3_loss/Neg_grad/NegEtraining/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/DynamicStitch*
T0*
Tshape0**
_class 
loc:@loss/dense_3_loss/Sum_1*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
ˇ
<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/TileTile?training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Reshape@training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/floordiv*

Tmultiples0*
T0**
_class 
loc:@loss/dense_3_loss/Sum_1*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ł
;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/ShapeShapedense_3_target*
T0*
out_type0*(
_class
loc:@loss/dense_3_loss/mul*
_output_shapes
:
ź
=training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Shape_1Shapeloss/dense_3_loss/Log*
_output_shapes
:*
T0*
out_type0*(
_class
loc:@loss/dense_3_loss/mul
Ç
Ktraining/RMSprop/gradients/loss/dense_3_loss/mul_grad/BroadcastGradientArgsBroadcastGradientArgs;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Shape=training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Shape_1*
T0*(
_class
loc:@loss/dense_3_loss/mul*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
ń
9training/RMSprop/gradients/loss/dense_3_loss/mul_grad/MulMul<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Tileloss/dense_3_loss/Log*
T0*(
_class
loc:@loss/dense_3_loss/mul*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
˛
9training/RMSprop/gradients/loss/dense_3_loss/mul_grad/SumSum9training/RMSprop/gradients/loss/dense_3_loss/mul_grad/MulKtraining/RMSprop/gradients/loss/dense_3_loss/mul_grad/BroadcastGradientArgs*
T0*(
_class
loc:@loss/dense_3_loss/mul*
_output_shapes
:*

Tidx0*
	keep_dims( 
ł
=training/RMSprop/gradients/loss/dense_3_loss/mul_grad/ReshapeReshape9training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Sum;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Shape*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
Tshape0*(
_class
loc:@loss/dense_3_loss/mul
ě
;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Mul_1Muldense_3_target<training/RMSprop/gradients/loss/dense_3_loss/Sum_1_grad/Tile*
T0*(
_class
loc:@loss/dense_3_loss/mul*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
¸
;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Sum_1Sum;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Mul_1Mtraining/RMSprop/gradients/loss/dense_3_loss/mul_grad/BroadcastGradientArgs:1*
_output_shapes
:*

Tidx0*
	keep_dims( *
T0*(
_class
loc:@loss/dense_3_loss/mul
°
?training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Reshape_1Reshape;training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Sum_1=training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Shape_1*
T0*
Tshape0*(
_class
loc:@loss/dense_3_loss/mul*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

@training/RMSprop/gradients/loss/dense_3_loss/Log_grad/Reciprocal
Reciprocalloss/dense_3_loss/clip_by_value@^training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Reshape_1*
T0*(
_class
loc:@loss/dense_3_loss/Log*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

9training/RMSprop/gradients/loss/dense_3_loss/Log_grad/mulMul?training/RMSprop/gradients/loss/dense_3_loss/mul_grad/Reshape_1@training/RMSprop/gradients/loss/dense_3_loss/Log_grad/Reciprocal*
T0*(
_class
loc:@loss/dense_3_loss/Log*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ŕ
Etraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/ShapeShape'loss/dense_3_loss/clip_by_value/Minimum*
T0*
out_type0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
_output_shapes
:
ž
Gtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape_1Const*
dtype0*
_output_shapes
: *
valueB *2
_class(
&$loc:@loss/dense_3_loss/clip_by_value
ô
Gtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape_2Shape9training/RMSprop/gradients/loss/dense_3_loss/Log_grad/mul*
T0*
out_type0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
_output_shapes
:
Ä
Ktraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/zeros/ConstConst*
valueB
 *    *2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
dtype0*
_output_shapes
: 
Ű
Etraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/zerosFillGtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape_2Ktraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/zeros/Const*
T0*

index_type0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

Ltraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/GreaterEqualGreaterEqual'loss/dense_3_loss/clip_by_value/Minimumloss/dense_3_loss/Const*
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ď
Utraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/BroadcastGradientArgsBroadcastGradientArgsEtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/ShapeGtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape_1*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙*
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value

Ftraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/SelectSelectLtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/GreaterEqual9training/RMSprop/gradients/loss/dense_3_loss/Log_grad/mulEtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/zeros*
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

Htraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Select_1SelectLtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/GreaterEqualEtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/zeros9training/RMSprop/gradients/loss/dense_3_loss/Log_grad/mul*
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ý
Ctraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/SumSumFtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/SelectUtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/BroadcastGradientArgs*
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ň
Gtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/ReshapeReshapeCtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/SumEtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape*
T0*
Tshape0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ă
Etraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Sum_1SumHtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Select_1Wtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
_output_shapes
:
Ç
Itraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Reshape_1ReshapeEtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Sum_1Gtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Shape_1*
T0*
Tshape0*2
_class(
&$loc:@loss/dense_3_loss/clip_by_value*
_output_shapes
: 
â
Mtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/ShapeShapeloss/dense_3_loss/truediv*
T0*
out_type0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
_output_shapes
:
Î
Otraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape_1Const*
valueB *:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
dtype0*
_output_shapes
: 

Otraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape_2ShapeGtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Reshape*
_output_shapes
:*
T0*
out_type0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum
Ô
Straining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/zeros/ConstConst*
valueB
 *    *:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
dtype0*
_output_shapes
: 
ű
Mtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/zerosFillOtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape_2Straining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/zeros/Const*
T0*

index_type0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ţ
Qtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/LessEqual	LessEqualloss/dense_3_loss/truedivloss/dense_3_loss/sub*
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

]training/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/BroadcastGradientArgsBroadcastGradientArgsMtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/ShapeOtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape_1*
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙
ą
Ntraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/SelectSelectQtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/LessEqualGtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/ReshapeMtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/zeros*
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ł
Ptraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Select_1SelectQtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/LessEqualMtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/zerosGtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value_grad/Reshape*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum
ý
Ktraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/SumSumNtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Select]training/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
_output_shapes
:
ň
Otraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/ReshapeReshapeKtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/SumMtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape*
T0*
Tshape0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

Mtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Sum_1SumPtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Select_1_training/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
_output_shapes
:
ç
Qtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Reshape_1ReshapeMtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Sum_1Otraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Shape_1*
T0*
Tshape0*:
_class0
.,loc:@loss/dense_3_loss/clip_by_value/Minimum*
_output_shapes
: 
ź
?training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/ShapeShapedense_3/Softmax*
T0*
out_type0*,
_class"
 loc:@loss/dense_3_loss/truediv*
_output_shapes
:
Ä
Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Shape_1Shapeloss/dense_3_loss/Sum*
T0*
out_type0*,
_class"
 loc:@loss/dense_3_loss/truediv*
_output_shapes
:
×
Otraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/BroadcastGradientArgsBroadcastGradientArgs?training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/ShapeAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Shape_1*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*2
_output_shapes 
:˙˙˙˙˙˙˙˙˙:˙˙˙˙˙˙˙˙˙

Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDivRealDivOtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/Reshapeloss/dense_3_loss/Sum*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ć
=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/SumSumAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDivOtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/BroadcastGradientArgs*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*
_output_shapes
:*

Tidx0*
	keep_dims( 
ş
Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/ReshapeReshape=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Sum?training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Shape*
T0*
Tshape0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ľ
=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/NegNegdense_3/Softmax*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv

Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDiv_1RealDiv=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Negloss/dense_3_loss/Sum*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDiv_2RealDivCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDiv_1loss/dense_3_loss/Sum*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
ş
=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/mulMulOtraining/RMSprop/gradients/loss/dense_3_loss/clip_by_value/Minimum_grad/ReshapeCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/RealDiv_2*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ć
?training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Sum_1Sum=training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/mulQtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/BroadcastGradientArgs:1*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv*
_output_shapes
:*

Tidx0*
	keep_dims( 
Ŕ
Ctraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Reshape_1Reshape?training/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Sum_1Atraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Shape_1*
T0*
Tshape0*,
_class"
 loc:@loss/dense_3_loss/truediv*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
´
;training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/ShapeShapedense_3/Softmax*
_output_shapes
:*
T0*
out_type0*(
_class
loc:@loss/dense_3_loss/Sum
Ś
:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/SizeConst*
value	B :*(
_class
loc:@loss/dense_3_loss/Sum*
dtype0*
_output_shapes
: 
đ
9training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/addAdd'loss/dense_3_loss/Sum/reduction_indices:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Size*
T0*(
_class
loc:@loss/dense_3_loss/Sum*
_output_shapes
: 

9training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/modFloorMod9training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/add:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Size*
T0*(
_class
loc:@loss/dense_3_loss/Sum*
_output_shapes
: 
Ş
=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Shape_1Const*
valueB *(
_class
loc:@loss/dense_3_loss/Sum*
dtype0*
_output_shapes
: 
­
Atraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/range/startConst*
dtype0*
_output_shapes
: *
value	B : *(
_class
loc:@loss/dense_3_loss/Sum
­
Atraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/range/deltaConst*
value	B :*(
_class
loc:@loss/dense_3_loss/Sum*
dtype0*
_output_shapes
: 
Ř
;training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/rangeRangeAtraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/range/start:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/SizeAtraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/range/delta*
_output_shapes
:*

Tidx0*(
_class
loc:@loss/dense_3_loss/Sum
Ź
@training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Fill/valueConst*
value	B :*(
_class
loc:@loss/dense_3_loss/Sum*
dtype0*
_output_shapes
: 
 
:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/FillFill=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Shape_1@training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Fill/value*
T0*

index_type0*(
_class
loc:@loss/dense_3_loss/Sum*
_output_shapes
: 

Ctraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/DynamicStitchDynamicStitch;training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/range9training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/mod;training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Shape:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Fill*
T0*(
_class
loc:@loss/dense_3_loss/Sum*
N*
_output_shapes
:
Ť
?training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Maximum/yConst*
value	B :*(
_class
loc:@loss/dense_3_loss/Sum*
dtype0*
_output_shapes
: 

=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/MaximumMaximumCtraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/DynamicStitch?training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Maximum/y*
_output_shapes
:*
T0*(
_class
loc:@loss/dense_3_loss/Sum

>training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/floordivFloorDiv;training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Shape=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Maximum*
_output_shapes
:*
T0*(
_class
loc:@loss/dense_3_loss/Sum
Ĺ
=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/ReshapeReshapeCtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Reshape_1Ctraining/RMSprop/gradients/loss/dense_3_loss/Sum_grad/DynamicStitch*
T0*
Tshape0*(
_class
loc:@loss/dense_3_loss/Sum*0
_output_shapes
:˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ż
:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/TileTile=training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Reshape>training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/floordiv*
T0*(
_class
loc:@loss/dense_3_loss/Sum*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tmultiples0

training/RMSprop/gradients/AddNAddNAtraining/RMSprop/gradients/loss/dense_3_loss/truediv_grad/Reshape:training/RMSprop/gradients/loss/dense_3_loss/Sum_grad/Tile*
N*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0*,
_class"
 loc:@loss/dense_3_loss/truediv
Â
3training/RMSprop/gradients/dense_3/Softmax_grad/mulMultraining/RMSprop/gradients/AddNdense_3/Softmax*
T0*"
_class
loc:@dense_3/Softmax*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
´
Etraining/RMSprop/gradients/dense_3/Softmax_grad/Sum/reduction_indicesConst*
dtype0*
_output_shapes
: *
valueB :
˙˙˙˙˙˙˙˙˙*"
_class
loc:@dense_3/Softmax
Š
3training/RMSprop/gradients/dense_3/Softmax_grad/SumSum3training/RMSprop/gradients/dense_3/Softmax_grad/mulEtraining/RMSprop/gradients/dense_3/Softmax_grad/Sum/reduction_indices*
T0*"
_class
loc:@dense_3/Softmax*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*

Tidx0*
	keep_dims(
ć
3training/RMSprop/gradients/dense_3/Softmax_grad/subSubtraining/RMSprop/gradients/AddN3training/RMSprop/gradients/dense_3/Softmax_grad/Sum*
T0*"
_class
loc:@dense_3/Softmax*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ř
5training/RMSprop/gradients/dense_3/Softmax_grad/mul_1Mul3training/RMSprop/gradients/dense_3/Softmax_grad/subdense_3/Softmax*
T0*"
_class
loc:@dense_3/Softmax*'
_output_shapes
:˙˙˙˙˙˙˙˙˙
á
;training/RMSprop/gradients/dense_3/BiasAdd_grad/BiasAddGradBiasAddGrad5training/RMSprop/gradients/dense_3/Softmax_grad/mul_1*
T0*"
_class
loc:@dense_3/BiasAdd*
data_formatNHWC*
_output_shapes
:

5training/RMSprop/gradients/dense_3/MatMul_grad/MatMulMatMul5training/RMSprop/gradients/dense_3/Softmax_grad/mul_1dense_3/MatMul/ReadVariableOp*
transpose_b(*
T0*!
_class
loc:@dense_3/MatMul*
transpose_a( *'
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
ţ
7training/RMSprop/gradients/dense_3/MatMul_grad/MatMul_1MatMuldropout_7/Identity5training/RMSprop/gradients/dense_3/Softmax_grad/mul_1*
transpose_a(*
_output_shapes

: *
transpose_b( *
T0*!
_class
loc:@dense_3/MatMul
č
;training/RMSprop/gradients/dense_2/Sigmoid_grad/SigmoidGradSigmoidGraddense_2/Sigmoid5training/RMSprop/gradients/dense_3/MatMul_grad/MatMul*'
_output_shapes
:˙˙˙˙˙˙˙˙˙ *
T0*"
_class
loc:@dense_2/Sigmoid
ç
;training/RMSprop/gradients/dense_2/BiasAdd_grad/BiasAddGradBiasAddGrad;training/RMSprop/gradients/dense_2/Sigmoid_grad/SigmoidGrad*
T0*"
_class
loc:@dense_2/BiasAdd*
data_formatNHWC*
_output_shapes
: 

5training/RMSprop/gradients/dense_2/MatMul_grad/MatMulMatMul;training/RMSprop/gradients/dense_2/Sigmoid_grad/SigmoidGraddense_2/MatMul/ReadVariableOp*
transpose_b(*
T0*!
_class
loc:@dense_2/MatMul*
transpose_a( *(
_output_shapes
:˙˙˙˙˙˙˙˙˙ŕ

7training/RMSprop/gradients/dense_2/MatMul_grad/MatMul_1MatMuldropout_6/Identity;training/RMSprop/gradients/dense_2/Sigmoid_grad/SigmoidGrad*
T0*!
_class
loc:@dense_2/MatMul*
transpose_a(*
_output_shapes
:	ŕ *
transpose_b( 
Ş
7training/RMSprop/gradients/flatten_1/Reshape_grad/ShapeShapeconv1d_5/Relu*
T0*
out_type0*$
_class
loc:@flatten_1/Reshape*
_output_shapes
:

9training/RMSprop/gradients/flatten_1/Reshape_grad/ReshapeReshape5training/RMSprop/gradients/dense_2/MatMul_grad/MatMul7training/RMSprop/gradients/flatten_1/Reshape_grad/Shape*
T0*
Tshape0*$
_class
loc:@flatten_1/Reshape*+
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
ä
6training/RMSprop/gradients/conv1d_5/Relu_grad/ReluGradReluGrad9training/RMSprop/gradients/flatten_1/Reshape_grad/Reshapeconv1d_5/Relu*
T0* 
_class
loc:@conv1d_5/Relu*+
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
ä
<training/RMSprop/gradients/conv1d_5/BiasAdd_grad/BiasAddGradBiasAddGrad6training/RMSprop/gradients/conv1d_5/Relu_grad/ReluGrad*
data_formatNHWC*
_output_shapes
: *
T0*#
_class
loc:@conv1d_5/BiasAdd
ż
=training/RMSprop/gradients/conv1d_5/conv1d/Squeeze_grad/ShapeShapeconv1d_5/conv1d/Conv2D*
T0*
out_type0**
_class 
loc:@conv1d_5/conv1d/Squeeze*
_output_shapes
:
ľ
?training/RMSprop/gradients/conv1d_5/conv1d/Squeeze_grad/ReshapeReshape6training/RMSprop/gradients/conv1d_5/Relu_grad/ReluGrad=training/RMSprop/gradients/conv1d_5/conv1d/Squeeze_grad/Shape*
T0*
Tshape0**
_class 
loc:@conv1d_5/conv1d/Squeeze*/
_output_shapes
:˙˙˙˙˙˙˙˙˙ 
đ
=training/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/ShapeNShapeNconv1d_5/conv1d/ExpandDimsconv1d_5/conv1d/ExpandDims_1*
T0*
out_type0*)
_class
loc:@conv1d_5/conv1d/Conv2D*
N* 
_output_shapes
::
Ó
Jtraining/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput=training/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/ShapeNconv1d_5/conv1d/ExpandDims_1?training/RMSprop/gradients/conv1d_5/conv1d/Squeeze_grad/Reshape*
	dilations
*
T0*)
_class
loc:@conv1d_5/conv1d/Conv2D*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*/
_output_shapes
:˙˙˙˙˙˙˙˙˙R@
Ě
Ktraining/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterconv1d_5/conv1d/ExpandDims?training/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/ShapeN:1?training/RMSprop/gradients/conv1d_5/conv1d/Squeeze_grad/Reshape*
	dilations
*
T0*)
_class
loc:@conv1d_5/conv1d/Conv2D*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID*&
_output_shapes
:@ 
Á
@training/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_grad/ShapeShapedropout_5/Identity*
T0*
out_type0*-
_class#
!loc:@conv1d_5/conv1d/ExpandDims*
_output_shapes
:
Î
Btraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_grad/ReshapeReshapeJtraining/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/Conv2DBackpropInput@training/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_grad/Shape*
T0*
Tshape0*-
_class#
!loc:@conv1d_5/conv1d/ExpandDims*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@
Č
Btraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_1_grad/ShapeConst*!
valueB"   @       */
_class%
#!loc:@conv1d_5/conv1d/ExpandDims_1*
dtype0*
_output_shapes
:
Ě
Dtraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_1_grad/ReshapeReshapeKtraining/RMSprop/gradients/conv1d_5/conv1d/Conv2D_grad/Conv2DBackpropFilterBtraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_1_grad/Shape*
T0*
Tshape0*/
_class%
#!loc:@conv1d_5/conv1d/ExpandDims_1*"
_output_shapes
:@ 
í
6training/RMSprop/gradients/conv1d_4/Relu_grad/ReluGradReluGradBtraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_grad/Reshapeconv1d_4/Relu*+
_output_shapes
:˙˙˙˙˙˙˙˙˙R@*
T0* 
_class
loc:@conv1d_4/Relu
ä
<training/RMSprop/gradients/conv1d_4/BiasAdd_grad/BiasAddGradBiasAddGrad6training/RMSprop/gradients/conv1d_4/Relu_grad/ReluGrad*
T0*#
_class
loc:@conv1d_4/BiasAdd*
data_formatNHWC*
_output_shapes
:@
ż
=training/RMSprop/gradients/conv1d_4/conv1d/Squeeze_grad/ShapeShapeconv1d_4/conv1d/Conv2D*
T0*
out_type0**
_class 
loc:@conv1d_4/conv1d/Squeeze*
_output_shapes
:
ľ
?training/RMSprop/gradients/conv1d_4/conv1d/Squeeze_grad/ReshapeReshape6training/RMSprop/gradients/conv1d_4/Relu_grad/ReluGrad=training/RMSprop/gradients/conv1d_4/conv1d/Squeeze_grad/Shape*
T0*
Tshape0**
_class 
loc:@conv1d_4/conv1d/Squeeze*/
_output_shapes
:˙˙˙˙˙˙˙˙˙R@
đ
=training/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/ShapeNShapeNconv1d_4/conv1d/ExpandDimsconv1d_4/conv1d/ExpandDims_1*
T0*
out_type0*)
_class
loc:@conv1d_4/conv1d/Conv2D*
N* 
_output_shapes
::
Ô
Jtraining/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput=training/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/ShapeNconv1d_4/conv1d/ExpandDims_1?training/RMSprop/gradients/conv1d_4/conv1d/Squeeze_grad/Reshape*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@*
	dilations
*
T0*)
_class
loc:@conv1d_4/conv1d/Conv2D*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingVALID
Ě
Ktraining/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterconv1d_4/conv1d/ExpandDims?training/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/ShapeN:1?training/RMSprop/gradients/conv1d_4/conv1d/Squeeze_grad/Reshape*&
_output_shapes
:@@*
	dilations
*
T0*)
_class
loc:@conv1d_4/conv1d/Conv2D*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingVALID
Á
@training/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_grad/ShapeShapedropout_4/Identity*
T0*
out_type0*-
_class#
!loc:@conv1d_4/conv1d/ExpandDims*
_output_shapes
:
Ď
Btraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_grad/ReshapeReshapeJtraining/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/Conv2DBackpropInput@training/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_grad/Shape*
T0*
Tshape0*-
_class#
!loc:@conv1d_4/conv1d/ExpandDims*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@
Č
Btraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_1_grad/ShapeConst*!
valueB"   @   @   */
_class%
#!loc:@conv1d_4/conv1d/ExpandDims_1*
dtype0*
_output_shapes
:
Ě
Dtraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_1_grad/ReshapeReshapeKtraining/RMSprop/gradients/conv1d_4/conv1d/Conv2D_grad/Conv2DBackpropFilterBtraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_1_grad/Shape*
T0*
Tshape0*/
_class%
#!loc:@conv1d_4/conv1d/ExpandDims_1*"
_output_shapes
:@@
î
6training/RMSprop/gradients/conv1d_3/Relu_grad/ReluGradReluGradBtraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_grad/Reshapeconv1d_3/Relu*
T0* 
_class
loc:@conv1d_3/Relu*,
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@
ä
<training/RMSprop/gradients/conv1d_3/BiasAdd_grad/BiasAddGradBiasAddGrad6training/RMSprop/gradients/conv1d_3/Relu_grad/ReluGrad*
T0*#
_class
loc:@conv1d_3/BiasAdd*
data_formatNHWC*
_output_shapes
:@
ż
=training/RMSprop/gradients/conv1d_3/conv1d/Squeeze_grad/ShapeShapeconv1d_3/conv1d/Conv2D*
_output_shapes
:*
T0*
out_type0**
_class 
loc:@conv1d_3/conv1d/Squeeze
ś
?training/RMSprop/gradients/conv1d_3/conv1d/Squeeze_grad/ReshapeReshape6training/RMSprop/gradients/conv1d_3/Relu_grad/ReluGrad=training/RMSprop/gradients/conv1d_3/conv1d/Squeeze_grad/Shape*
T0*
Tshape0**
_class 
loc:@conv1d_3/conv1d/Squeeze*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ř@
đ
=training/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/ShapeNShapeNconv1d_3/conv1d/ExpandDimsconv1d_3/conv1d/ExpandDims_1*
N* 
_output_shapes
::*
T0*
out_type0*)
_class
loc:@conv1d_3/conv1d/Conv2D
Ô
Jtraining/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInput=training/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/ShapeNconv1d_3/conv1d/ExpandDims_1?training/RMSprop/gradients/conv1d_3/conv1d/Squeeze_grad/Reshape*
paddingVALID*0
_output_shapes
:˙˙˙˙˙˙˙˙˙ú*
	dilations
*
T0*)
_class
loc:@conv1d_3/conv1d/Conv2D*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(
Ě
Ktraining/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFilterconv1d_3/conv1d/ExpandDims?training/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/ShapeN:1?training/RMSprop/gradients/conv1d_3/conv1d/Squeeze_grad/Reshape*
paddingVALID*&
_output_shapes
:@*
	dilations
*
T0*)
_class
loc:@conv1d_3/conv1d/Conv2D*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
Č
Btraining/RMSprop/gradients/conv1d_3/conv1d/ExpandDims_1_grad/ShapeConst*!
valueB"      @   */
_class%
#!loc:@conv1d_3/conv1d/ExpandDims_1*
dtype0*
_output_shapes
:
Ě
Dtraining/RMSprop/gradients/conv1d_3/conv1d/ExpandDims_1_grad/ReshapeReshapeKtraining/RMSprop/gradients/conv1d_3/conv1d/Conv2D_grad/Conv2DBackpropFilterBtraining/RMSprop/gradients/conv1d_3/conv1d/ExpandDims_1_grad/Shape*
T0*
Tshape0*/
_class%
#!loc:@conv1d_3/conv1d/ExpandDims_1*"
_output_shapes
:@
{
&training/RMSprop/zeros/shape_as_tensorConst*
dtype0*
_output_shapes
:*!
valueB"      @   
a
training/RMSprop/zeros/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
Ł
training/RMSprop/zerosFill&training/RMSprop/zeros/shape_as_tensortraining/RMSprop/zeros/Const*
T0*

index_type0*"
_output_shapes
:@
Ń
training/RMSprop/VariableVarHandleOp*
shape:@*
dtype0*
_output_shapes
: **
shared_nametraining/RMSprop/Variable*,
_class"
 loc:@training/RMSprop/Variable*
	container 

:training/RMSprop/Variable/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable*
_output_shapes
: 
˘
 training/RMSprop/Variable/AssignAssignVariableOptraining/RMSprop/Variabletraining/RMSprop/zeros*,
_class"
 loc:@training/RMSprop/Variable*
dtype0
š
-training/RMSprop/Variable/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable*
dtype0*"
_output_shapes
:@*,
_class"
 loc:@training/RMSprop/Variable
e
training/RMSprop/zeros_1Const*
valueB@*    *
dtype0*
_output_shapes
:@
Ď
training/RMSprop/Variable_1VarHandleOp*,
shared_nametraining/RMSprop/Variable_1*.
_class$
" loc:@training/RMSprop/Variable_1*
	container *
shape:@*
dtype0*
_output_shapes
: 

<training/RMSprop/Variable_1/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_1*
_output_shapes
: 
Ş
"training/RMSprop/Variable_1/AssignAssignVariableOptraining/RMSprop/Variable_1training/RMSprop/zeros_1*.
_class$
" loc:@training/RMSprop/Variable_1*
dtype0
ˇ
/training/RMSprop/Variable_1/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_1*.
_class$
" loc:@training/RMSprop/Variable_1*
dtype0*
_output_shapes
:@
}
(training/RMSprop/zeros_2/shape_as_tensorConst*!
valueB"   @   @   *
dtype0*
_output_shapes
:
c
training/RMSprop/zeros_2/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *    
Š
training/RMSprop/zeros_2Fill(training/RMSprop/zeros_2/shape_as_tensortraining/RMSprop/zeros_2/Const*"
_output_shapes
:@@*
T0*

index_type0
×
training/RMSprop/Variable_2VarHandleOp*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_2*.
_class$
" loc:@training/RMSprop/Variable_2*
	container *
shape:@@

<training/RMSprop/Variable_2/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_2*
_output_shapes
: 
Ş
"training/RMSprop/Variable_2/AssignAssignVariableOptraining/RMSprop/Variable_2training/RMSprop/zeros_2*
dtype0*.
_class$
" loc:@training/RMSprop/Variable_2
ż
/training/RMSprop/Variable_2/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_2*.
_class$
" loc:@training/RMSprop/Variable_2*
dtype0*"
_output_shapes
:@@
e
training/RMSprop/zeros_3Const*
valueB@*    *
dtype0*
_output_shapes
:@
Ď
training/RMSprop/Variable_3VarHandleOp*
shape:@*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_3*.
_class$
" loc:@training/RMSprop/Variable_3*
	container 

<training/RMSprop/Variable_3/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_3*
_output_shapes
: 
Ş
"training/RMSprop/Variable_3/AssignAssignVariableOptraining/RMSprop/Variable_3training/RMSprop/zeros_3*.
_class$
" loc:@training/RMSprop/Variable_3*
dtype0
ˇ
/training/RMSprop/Variable_3/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_3*.
_class$
" loc:@training/RMSprop/Variable_3*
dtype0*
_output_shapes
:@
}
(training/RMSprop/zeros_4/shape_as_tensorConst*!
valueB"   @       *
dtype0*
_output_shapes
:
c
training/RMSprop/zeros_4/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Š
training/RMSprop/zeros_4Fill(training/RMSprop/zeros_4/shape_as_tensortraining/RMSprop/zeros_4/Const*
T0*

index_type0*"
_output_shapes
:@ 
×
training/RMSprop/Variable_4VarHandleOp*,
shared_nametraining/RMSprop/Variable_4*.
_class$
" loc:@training/RMSprop/Variable_4*
	container *
shape:@ *
dtype0*
_output_shapes
: 

<training/RMSprop/Variable_4/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_4*
_output_shapes
: 
Ş
"training/RMSprop/Variable_4/AssignAssignVariableOptraining/RMSprop/Variable_4training/RMSprop/zeros_4*
dtype0*.
_class$
" loc:@training/RMSprop/Variable_4
ż
/training/RMSprop/Variable_4/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_4*
dtype0*"
_output_shapes
:@ *.
_class$
" loc:@training/RMSprop/Variable_4
e
training/RMSprop/zeros_5Const*
valueB *    *
dtype0*
_output_shapes
: 
Ď
training/RMSprop/Variable_5VarHandleOp*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_5*.
_class$
" loc:@training/RMSprop/Variable_5*
	container *
shape: 

<training/RMSprop/Variable_5/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_5*
_output_shapes
: 
Ş
"training/RMSprop/Variable_5/AssignAssignVariableOptraining/RMSprop/Variable_5training/RMSprop/zeros_5*.
_class$
" loc:@training/RMSprop/Variable_5*
dtype0
ˇ
/training/RMSprop/Variable_5/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_5*.
_class$
" loc:@training/RMSprop/Variable_5*
dtype0*
_output_shapes
: 
y
(training/RMSprop/zeros_6/shape_as_tensorConst*
valueB"`      *
dtype0*
_output_shapes
:
c
training/RMSprop/zeros_6/ConstConst*
valueB
 *    *
dtype0*
_output_shapes
: 
Ś
training/RMSprop/zeros_6Fill(training/RMSprop/zeros_6/shape_as_tensortraining/RMSprop/zeros_6/Const*
_output_shapes
:	ŕ *
T0*

index_type0
Ô
training/RMSprop/Variable_6VarHandleOp*,
shared_nametraining/RMSprop/Variable_6*.
_class$
" loc:@training/RMSprop/Variable_6*
	container *
shape:	ŕ *
dtype0*
_output_shapes
: 

<training/RMSprop/Variable_6/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_6*
_output_shapes
: 
Ş
"training/RMSprop/Variable_6/AssignAssignVariableOptraining/RMSprop/Variable_6training/RMSprop/zeros_6*
dtype0*.
_class$
" loc:@training/RMSprop/Variable_6
ź
/training/RMSprop/Variable_6/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_6*
dtype0*
_output_shapes
:	ŕ *.
_class$
" loc:@training/RMSprop/Variable_6
e
training/RMSprop/zeros_7Const*
dtype0*
_output_shapes
: *
valueB *    
Ď
training/RMSprop/Variable_7VarHandleOp*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_7*.
_class$
" loc:@training/RMSprop/Variable_7*
	container *
shape: 

<training/RMSprop/Variable_7/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_7*
_output_shapes
: 
Ş
"training/RMSprop/Variable_7/AssignAssignVariableOptraining/RMSprop/Variable_7training/RMSprop/zeros_7*
dtype0*.
_class$
" loc:@training/RMSprop/Variable_7
ˇ
/training/RMSprop/Variable_7/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_7*.
_class$
" loc:@training/RMSprop/Variable_7*
dtype0*
_output_shapes
: 
m
training/RMSprop/zeros_8Const*
valueB *    *
dtype0*
_output_shapes

: 
Ó
training/RMSprop/Variable_8VarHandleOp*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_8*.
_class$
" loc:@training/RMSprop/Variable_8*
	container *
shape
: 

<training/RMSprop/Variable_8/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_8*
_output_shapes
: 
Ş
"training/RMSprop/Variable_8/AssignAssignVariableOptraining/RMSprop/Variable_8training/RMSprop/zeros_8*.
_class$
" loc:@training/RMSprop/Variable_8*
dtype0
ť
/training/RMSprop/Variable_8/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_8*
dtype0*
_output_shapes

: *.
_class$
" loc:@training/RMSprop/Variable_8
e
training/RMSprop/zeros_9Const*
valueB*    *
dtype0*
_output_shapes
:
Ď
training/RMSprop/Variable_9VarHandleOp*.
_class$
" loc:@training/RMSprop/Variable_9*
	container *
shape:*
dtype0*
_output_shapes
: *,
shared_nametraining/RMSprop/Variable_9

<training/RMSprop/Variable_9/IsInitialized/VarIsInitializedOpVarIsInitializedOptraining/RMSprop/Variable_9*
_output_shapes
: 
Ş
"training/RMSprop/Variable_9/AssignAssignVariableOptraining/RMSprop/Variable_9training/RMSprop/zeros_9*.
_class$
" loc:@training/RMSprop/Variable_9*
dtype0
ˇ
/training/RMSprop/Variable_9/Read/ReadVariableOpReadVariableOptraining/RMSprop/Variable_9*.
_class$
" loc:@training/RMSprop/Variable_9*
dtype0*
_output_shapes
:
X
training/RMSprop/ConstConst*
value	B	 R*
dtype0	*
_output_shapes
: 
t
$training/RMSprop/AssignAddVariableOpAssignAddVariableOpRMSprop/iterationstraining/RMSprop/Const*
dtype0	

training/RMSprop/ReadVariableOpReadVariableOpRMSprop/iterations%^training/RMSprop/AssignAddVariableOp*
dtype0	*
_output_shapes
: 
e
!training/RMSprop/ReadVariableOp_1ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 

#training/RMSprop/mul/ReadVariableOpReadVariableOptraining/RMSprop/Variable*
dtype0*"
_output_shapes
:@

training/RMSprop/mulMul!training/RMSprop/ReadVariableOp_1#training/RMSprop/mul/ReadVariableOp*"
_output_shapes
:@*
T0
e
!training/RMSprop/ReadVariableOp_2ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
[
training/RMSprop/sub/xConst*
dtype0*
_output_shapes
: *
valueB
 *  ?
w
training/RMSprop/subSubtraining/RMSprop/sub/x!training/RMSprop/ReadVariableOp_2*
T0*
_output_shapes
: 

training/RMSprop/SquareSquareDtraining/RMSprop/gradients/conv1d_3/conv1d/ExpandDims_1_grad/Reshape*
T0*"
_output_shapes
:@
y
training/RMSprop/mul_1Multraining/RMSprop/subtraining/RMSprop/Square*
T0*"
_output_shapes
:@
v
training/RMSprop/addAddtraining/RMSprop/multraining/RMSprop/mul_1*
T0*"
_output_shapes
:@
s
!training/RMSprop/AssignVariableOpAssignVariableOptraining/RMSprop/Variabletraining/RMSprop/add*
dtype0
Ł
!training/RMSprop/ReadVariableOp_3ReadVariableOptraining/RMSprop/Variable"^training/RMSprop/AssignVariableOp*
dtype0*"
_output_shapes
:@
d
!training/RMSprop/ReadVariableOp_4ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
ł
training/RMSprop/mul_2Mul!training/RMSprop/ReadVariableOp_4Dtraining/RMSprop/gradients/conv1d_3/conv1d/ExpandDims_1_grad/Reshape*
T0*"
_output_shapes
:@
]
training/RMSprop/Const_1Const*
valueB
 *    *
dtype0*
_output_shapes
: 
]
training/RMSprop/Const_2Const*
valueB
 *  *
dtype0*
_output_shapes
: 

&training/RMSprop/clip_by_value/MinimumMinimumtraining/RMSprop/addtraining/RMSprop/Const_2*
T0*"
_output_shapes
:@

training/RMSprop/clip_by_valueMaximum&training/RMSprop/clip_by_value/Minimumtraining/RMSprop/Const_1*
T0*"
_output_shapes
:@
j
training/RMSprop/SqrtSqrttraining/RMSprop/clip_by_value*
T0*"
_output_shapes
:@
]
training/RMSprop/add_1/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
{
training/RMSprop/add_1Addtraining/RMSprop/Sqrttraining/RMSprop/add_1/y*
T0*"
_output_shapes
:@

training/RMSprop/truedivRealDivtraining/RMSprop/mul_2training/RMSprop/add_1*
T0*"
_output_shapes
:@
u
!training/RMSprop/ReadVariableOp_5ReadVariableOpconv1d_3/kernel*
dtype0*"
_output_shapes
:@

training/RMSprop/sub_1Sub!training/RMSprop/ReadVariableOp_5training/RMSprop/truediv*
T0*"
_output_shapes
:@
m
#training/RMSprop/AssignVariableOp_1AssignVariableOpconv1d_3/kerneltraining/RMSprop/sub_1*
dtype0

!training/RMSprop/ReadVariableOp_6ReadVariableOpconv1d_3/kernel$^training/RMSprop/AssignVariableOp_1*
dtype0*"
_output_shapes
:@
e
!training/RMSprop/ReadVariableOp_7ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
}
%training/RMSprop/mul_3/ReadVariableOpReadVariableOptraining/RMSprop/Variable_1*
dtype0*
_output_shapes
:@

training/RMSprop/mul_3Mul!training/RMSprop/ReadVariableOp_7%training/RMSprop/mul_3/ReadVariableOp*
T0*
_output_shapes
:@
e
!training/RMSprop/ReadVariableOp_8ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
]
training/RMSprop/sub_2/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
{
training/RMSprop/sub_2Subtraining/RMSprop/sub_2/x!training/RMSprop/ReadVariableOp_8*
T0*
_output_shapes
: 

training/RMSprop/Square_1Square<training/RMSprop/gradients/conv1d_3/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:@
u
training/RMSprop/mul_4Multraining/RMSprop/sub_2training/RMSprop/Square_1*
T0*
_output_shapes
:@
r
training/RMSprop/add_2Addtraining/RMSprop/mul_3training/RMSprop/mul_4*
_output_shapes
:@*
T0
y
#training/RMSprop/AssignVariableOp_2AssignVariableOptraining/RMSprop/Variable_1training/RMSprop/add_2*
dtype0

!training/RMSprop/ReadVariableOp_9ReadVariableOptraining/RMSprop/Variable_1$^training/RMSprop/AssignVariableOp_2*
dtype0*
_output_shapes
:@
e
"training/RMSprop/ReadVariableOp_10ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
¤
training/RMSprop/mul_5Mul"training/RMSprop/ReadVariableOp_10<training/RMSprop/gradients/conv1d_3/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:@
]
training/RMSprop/Const_3Const*
valueB
 *    *
dtype0*
_output_shapes
: 
]
training/RMSprop/Const_4Const*
dtype0*
_output_shapes
: *
valueB
 *  

(training/RMSprop/clip_by_value_1/MinimumMinimumtraining/RMSprop/add_2training/RMSprop/Const_4*
T0*
_output_shapes
:@

 training/RMSprop/clip_by_value_1Maximum(training/RMSprop/clip_by_value_1/Minimumtraining/RMSprop/Const_3*
T0*
_output_shapes
:@
f
training/RMSprop/Sqrt_1Sqrt training/RMSprop/clip_by_value_1*
_output_shapes
:@*
T0
]
training/RMSprop/add_3/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
u
training/RMSprop/add_3Addtraining/RMSprop/Sqrt_1training/RMSprop/add_3/y*
T0*
_output_shapes
:@
z
training/RMSprop/truediv_1RealDivtraining/RMSprop/mul_5training/RMSprop/add_3*
_output_shapes
:@*
T0
l
"training/RMSprop/ReadVariableOp_11ReadVariableOpconv1d_3/bias*
dtype0*
_output_shapes
:@

training/RMSprop/sub_3Sub"training/RMSprop/ReadVariableOp_11training/RMSprop/truediv_1*
T0*
_output_shapes
:@
k
#training/RMSprop/AssignVariableOp_3AssignVariableOpconv1d_3/biastraining/RMSprop/sub_3*
dtype0

"training/RMSprop/ReadVariableOp_12ReadVariableOpconv1d_3/bias$^training/RMSprop/AssignVariableOp_3*
dtype0*
_output_shapes
:@
f
"training/RMSprop/ReadVariableOp_13ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 

%training/RMSprop/mul_6/ReadVariableOpReadVariableOptraining/RMSprop/Variable_2*
dtype0*"
_output_shapes
:@@

training/RMSprop/mul_6Mul"training/RMSprop/ReadVariableOp_13%training/RMSprop/mul_6/ReadVariableOp*"
_output_shapes
:@@*
T0
f
"training/RMSprop/ReadVariableOp_14ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
]
training/RMSprop/sub_4/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
|
training/RMSprop/sub_4Subtraining/RMSprop/sub_4/x"training/RMSprop/ReadVariableOp_14*
T0*
_output_shapes
: 

training/RMSprop/Square_2SquareDtraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_1_grad/Reshape*
T0*"
_output_shapes
:@@
}
training/RMSprop/mul_7Multraining/RMSprop/sub_4training/RMSprop/Square_2*
T0*"
_output_shapes
:@@
z
training/RMSprop/add_4Addtraining/RMSprop/mul_6training/RMSprop/mul_7*
T0*"
_output_shapes
:@@
y
#training/RMSprop/AssignVariableOp_4AssignVariableOptraining/RMSprop/Variable_2training/RMSprop/add_4*
dtype0
¨
"training/RMSprop/ReadVariableOp_15ReadVariableOptraining/RMSprop/Variable_2$^training/RMSprop/AssignVariableOp_4*
dtype0*"
_output_shapes
:@@
e
"training/RMSprop/ReadVariableOp_16ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
´
training/RMSprop/mul_8Mul"training/RMSprop/ReadVariableOp_16Dtraining/RMSprop/gradients/conv1d_4/conv1d/ExpandDims_1_grad/Reshape*"
_output_shapes
:@@*
T0
]
training/RMSprop/Const_5Const*
valueB
 *    *
dtype0*
_output_shapes
: 
]
training/RMSprop/Const_6Const*
valueB
 *  *
dtype0*
_output_shapes
: 

(training/RMSprop/clip_by_value_2/MinimumMinimumtraining/RMSprop/add_4training/RMSprop/Const_6*"
_output_shapes
:@@*
T0

 training/RMSprop/clip_by_value_2Maximum(training/RMSprop/clip_by_value_2/Minimumtraining/RMSprop/Const_5*
T0*"
_output_shapes
:@@
n
training/RMSprop/Sqrt_2Sqrt training/RMSprop/clip_by_value_2*
T0*"
_output_shapes
:@@
]
training/RMSprop/add_5/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
}
training/RMSprop/add_5Addtraining/RMSprop/Sqrt_2training/RMSprop/add_5/y*
T0*"
_output_shapes
:@@

training/RMSprop/truediv_2RealDivtraining/RMSprop/mul_8training/RMSprop/add_5*"
_output_shapes
:@@*
T0
v
"training/RMSprop/ReadVariableOp_17ReadVariableOpconv1d_4/kernel*
dtype0*"
_output_shapes
:@@

training/RMSprop/sub_5Sub"training/RMSprop/ReadVariableOp_17training/RMSprop/truediv_2*"
_output_shapes
:@@*
T0
m
#training/RMSprop/AssignVariableOp_5AssignVariableOpconv1d_4/kerneltraining/RMSprop/sub_5*
dtype0

"training/RMSprop/ReadVariableOp_18ReadVariableOpconv1d_4/kernel$^training/RMSprop/AssignVariableOp_5*
dtype0*"
_output_shapes
:@@
f
"training/RMSprop/ReadVariableOp_19ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
}
%training/RMSprop/mul_9/ReadVariableOpReadVariableOptraining/RMSprop/Variable_3*
dtype0*
_output_shapes
:@

training/RMSprop/mul_9Mul"training/RMSprop/ReadVariableOp_19%training/RMSprop/mul_9/ReadVariableOp*
T0*
_output_shapes
:@
f
"training/RMSprop/ReadVariableOp_20ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
]
training/RMSprop/sub_6/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
|
training/RMSprop/sub_6Subtraining/RMSprop/sub_6/x"training/RMSprop/ReadVariableOp_20*
T0*
_output_shapes
: 

training/RMSprop/Square_3Square<training/RMSprop/gradients/conv1d_4/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:@
v
training/RMSprop/mul_10Multraining/RMSprop/sub_6training/RMSprop/Square_3*
T0*
_output_shapes
:@
s
training/RMSprop/add_6Addtraining/RMSprop/mul_9training/RMSprop/mul_10*
_output_shapes
:@*
T0
y
#training/RMSprop/AssignVariableOp_6AssignVariableOptraining/RMSprop/Variable_3training/RMSprop/add_6*
dtype0
 
"training/RMSprop/ReadVariableOp_21ReadVariableOptraining/RMSprop/Variable_3$^training/RMSprop/AssignVariableOp_6*
dtype0*
_output_shapes
:@
e
"training/RMSprop/ReadVariableOp_22ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
Ľ
training/RMSprop/mul_11Mul"training/RMSprop/ReadVariableOp_22<training/RMSprop/gradients/conv1d_4/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:@
]
training/RMSprop/Const_7Const*
valueB
 *    *
dtype0*
_output_shapes
: 
]
training/RMSprop/Const_8Const*
valueB
 *  *
dtype0*
_output_shapes
: 

(training/RMSprop/clip_by_value_3/MinimumMinimumtraining/RMSprop/add_6training/RMSprop/Const_8*
T0*
_output_shapes
:@

 training/RMSprop/clip_by_value_3Maximum(training/RMSprop/clip_by_value_3/Minimumtraining/RMSprop/Const_7*
T0*
_output_shapes
:@
f
training/RMSprop/Sqrt_3Sqrt training/RMSprop/clip_by_value_3*
T0*
_output_shapes
:@
]
training/RMSprop/add_7/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
u
training/RMSprop/add_7Addtraining/RMSprop/Sqrt_3training/RMSprop/add_7/y*
T0*
_output_shapes
:@
{
training/RMSprop/truediv_3RealDivtraining/RMSprop/mul_11training/RMSprop/add_7*
T0*
_output_shapes
:@
l
"training/RMSprop/ReadVariableOp_23ReadVariableOpconv1d_4/bias*
dtype0*
_output_shapes
:@

training/RMSprop/sub_7Sub"training/RMSprop/ReadVariableOp_23training/RMSprop/truediv_3*
T0*
_output_shapes
:@
k
#training/RMSprop/AssignVariableOp_7AssignVariableOpconv1d_4/biastraining/RMSprop/sub_7*
dtype0

"training/RMSprop/ReadVariableOp_24ReadVariableOpconv1d_4/bias$^training/RMSprop/AssignVariableOp_7*
dtype0*
_output_shapes
:@
f
"training/RMSprop/ReadVariableOp_25ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 

&training/RMSprop/mul_12/ReadVariableOpReadVariableOptraining/RMSprop/Variable_4*
dtype0*"
_output_shapes
:@ 

training/RMSprop/mul_12Mul"training/RMSprop/ReadVariableOp_25&training/RMSprop/mul_12/ReadVariableOp*"
_output_shapes
:@ *
T0
f
"training/RMSprop/ReadVariableOp_26ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
]
training/RMSprop/sub_8/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
|
training/RMSprop/sub_8Subtraining/RMSprop/sub_8/x"training/RMSprop/ReadVariableOp_26*
T0*
_output_shapes
: 

training/RMSprop/Square_4SquareDtraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_1_grad/Reshape*
T0*"
_output_shapes
:@ 
~
training/RMSprop/mul_13Multraining/RMSprop/sub_8training/RMSprop/Square_4*
T0*"
_output_shapes
:@ 
|
training/RMSprop/add_8Addtraining/RMSprop/mul_12training/RMSprop/mul_13*
T0*"
_output_shapes
:@ 
y
#training/RMSprop/AssignVariableOp_8AssignVariableOptraining/RMSprop/Variable_4training/RMSprop/add_8*
dtype0
¨
"training/RMSprop/ReadVariableOp_27ReadVariableOptraining/RMSprop/Variable_4$^training/RMSprop/AssignVariableOp_8*
dtype0*"
_output_shapes
:@ 
e
"training/RMSprop/ReadVariableOp_28ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
ľ
training/RMSprop/mul_14Mul"training/RMSprop/ReadVariableOp_28Dtraining/RMSprop/gradients/conv1d_5/conv1d/ExpandDims_1_grad/Reshape*"
_output_shapes
:@ *
T0
]
training/RMSprop/Const_9Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_10Const*
valueB
 *  *
dtype0*
_output_shapes
: 

(training/RMSprop/clip_by_value_4/MinimumMinimumtraining/RMSprop/add_8training/RMSprop/Const_10*"
_output_shapes
:@ *
T0

 training/RMSprop/clip_by_value_4Maximum(training/RMSprop/clip_by_value_4/Minimumtraining/RMSprop/Const_9*
T0*"
_output_shapes
:@ 
n
training/RMSprop/Sqrt_4Sqrt training/RMSprop/clip_by_value_4*
T0*"
_output_shapes
:@ 
]
training/RMSprop/add_9/yConst*
dtype0*
_output_shapes
: *
valueB
 *żÖ3
}
training/RMSprop/add_9Addtraining/RMSprop/Sqrt_4training/RMSprop/add_9/y*
T0*"
_output_shapes
:@ 

training/RMSprop/truediv_4RealDivtraining/RMSprop/mul_14training/RMSprop/add_9*"
_output_shapes
:@ *
T0
v
"training/RMSprop/ReadVariableOp_29ReadVariableOpconv1d_5/kernel*
dtype0*"
_output_shapes
:@ 

training/RMSprop/sub_9Sub"training/RMSprop/ReadVariableOp_29training/RMSprop/truediv_4*
T0*"
_output_shapes
:@ 
m
#training/RMSprop/AssignVariableOp_9AssignVariableOpconv1d_5/kerneltraining/RMSprop/sub_9*
dtype0

"training/RMSprop/ReadVariableOp_30ReadVariableOpconv1d_5/kernel$^training/RMSprop/AssignVariableOp_9*
dtype0*"
_output_shapes
:@ 
f
"training/RMSprop/ReadVariableOp_31ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
~
&training/RMSprop/mul_15/ReadVariableOpReadVariableOptraining/RMSprop/Variable_5*
dtype0*
_output_shapes
: 

training/RMSprop/mul_15Mul"training/RMSprop/ReadVariableOp_31&training/RMSprop/mul_15/ReadVariableOp*
T0*
_output_shapes
: 
f
"training/RMSprop/ReadVariableOp_32ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
^
training/RMSprop/sub_10/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
~
training/RMSprop/sub_10Subtraining/RMSprop/sub_10/x"training/RMSprop/ReadVariableOp_32*
_output_shapes
: *
T0

training/RMSprop/Square_5Square<training/RMSprop/gradients/conv1d_5/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
: 
w
training/RMSprop/mul_16Multraining/RMSprop/sub_10training/RMSprop/Square_5*
T0*
_output_shapes
: 
u
training/RMSprop/add_10Addtraining/RMSprop/mul_15training/RMSprop/mul_16*
T0*
_output_shapes
: 
{
$training/RMSprop/AssignVariableOp_10AssignVariableOptraining/RMSprop/Variable_5training/RMSprop/add_10*
dtype0
Ą
"training/RMSprop/ReadVariableOp_33ReadVariableOptraining/RMSprop/Variable_5%^training/RMSprop/AssignVariableOp_10*
dtype0*
_output_shapes
: 
e
"training/RMSprop/ReadVariableOp_34ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
Ľ
training/RMSprop/mul_17Mul"training/RMSprop/ReadVariableOp_34<training/RMSprop/gradients/conv1d_5/BiasAdd_grad/BiasAddGrad*
_output_shapes
: *
T0
^
training/RMSprop/Const_11Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_12Const*
valueB
 *  *
dtype0*
_output_shapes
: 

(training/RMSprop/clip_by_value_5/MinimumMinimumtraining/RMSprop/add_10training/RMSprop/Const_12*
T0*
_output_shapes
: 

 training/RMSprop/clip_by_value_5Maximum(training/RMSprop/clip_by_value_5/Minimumtraining/RMSprop/Const_11*
T0*
_output_shapes
: 
f
training/RMSprop/Sqrt_5Sqrt training/RMSprop/clip_by_value_5*
T0*
_output_shapes
: 
^
training/RMSprop/add_11/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
w
training/RMSprop/add_11Addtraining/RMSprop/Sqrt_5training/RMSprop/add_11/y*
T0*
_output_shapes
: 
|
training/RMSprop/truediv_5RealDivtraining/RMSprop/mul_17training/RMSprop/add_11*
T0*
_output_shapes
: 
l
"training/RMSprop/ReadVariableOp_35ReadVariableOpconv1d_5/bias*
dtype0*
_output_shapes
: 

training/RMSprop/sub_11Sub"training/RMSprop/ReadVariableOp_35training/RMSprop/truediv_5*
T0*
_output_shapes
: 
m
$training/RMSprop/AssignVariableOp_11AssignVariableOpconv1d_5/biastraining/RMSprop/sub_11*
dtype0

"training/RMSprop/ReadVariableOp_36ReadVariableOpconv1d_5/bias%^training/RMSprop/AssignVariableOp_11*
dtype0*
_output_shapes
: 
f
"training/RMSprop/ReadVariableOp_37ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 

&training/RMSprop/mul_18/ReadVariableOpReadVariableOptraining/RMSprop/Variable_6*
dtype0*
_output_shapes
:	ŕ 

training/RMSprop/mul_18Mul"training/RMSprop/ReadVariableOp_37&training/RMSprop/mul_18/ReadVariableOp*
_output_shapes
:	ŕ *
T0
f
"training/RMSprop/ReadVariableOp_38ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
^
training/RMSprop/sub_12/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
~
training/RMSprop/sub_12Subtraining/RMSprop/sub_12/x"training/RMSprop/ReadVariableOp_38*
_output_shapes
: *
T0

training/RMSprop/Square_6Square7training/RMSprop/gradients/dense_2/MatMul_grad/MatMul_1*
T0*
_output_shapes
:	ŕ 
|
training/RMSprop/mul_19Multraining/RMSprop/sub_12training/RMSprop/Square_6*
T0*
_output_shapes
:	ŕ 
z
training/RMSprop/add_12Addtraining/RMSprop/mul_18training/RMSprop/mul_19*
T0*
_output_shapes
:	ŕ 
{
$training/RMSprop/AssignVariableOp_12AssignVariableOptraining/RMSprop/Variable_6training/RMSprop/add_12*
dtype0
Ś
"training/RMSprop/ReadVariableOp_39ReadVariableOptraining/RMSprop/Variable_6%^training/RMSprop/AssignVariableOp_12*
dtype0*
_output_shapes
:	ŕ 
e
"training/RMSprop/ReadVariableOp_40ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
Ľ
training/RMSprop/mul_20Mul"training/RMSprop/ReadVariableOp_407training/RMSprop/gradients/dense_2/MatMul_grad/MatMul_1*
T0*
_output_shapes
:	ŕ 
^
training/RMSprop/Const_13Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_14Const*
dtype0*
_output_shapes
: *
valueB
 *  

(training/RMSprop/clip_by_value_6/MinimumMinimumtraining/RMSprop/add_12training/RMSprop/Const_14*
_output_shapes
:	ŕ *
T0

 training/RMSprop/clip_by_value_6Maximum(training/RMSprop/clip_by_value_6/Minimumtraining/RMSprop/Const_13*
T0*
_output_shapes
:	ŕ 
k
training/RMSprop/Sqrt_6Sqrt training/RMSprop/clip_by_value_6*
T0*
_output_shapes
:	ŕ 
^
training/RMSprop/add_13/yConst*
dtype0*
_output_shapes
: *
valueB
 *żÖ3
|
training/RMSprop/add_13Addtraining/RMSprop/Sqrt_6training/RMSprop/add_13/y*
_output_shapes
:	ŕ *
T0

training/RMSprop/truediv_6RealDivtraining/RMSprop/mul_20training/RMSprop/add_13*
_output_shapes
:	ŕ *
T0
r
"training/RMSprop/ReadVariableOp_41ReadVariableOpdense_2/kernel*
dtype0*
_output_shapes
:	ŕ 

training/RMSprop/sub_13Sub"training/RMSprop/ReadVariableOp_41training/RMSprop/truediv_6*
_output_shapes
:	ŕ *
T0
n
$training/RMSprop/AssignVariableOp_13AssignVariableOpdense_2/kerneltraining/RMSprop/sub_13*
dtype0

"training/RMSprop/ReadVariableOp_42ReadVariableOpdense_2/kernel%^training/RMSprop/AssignVariableOp_13*
dtype0*
_output_shapes
:	ŕ 
f
"training/RMSprop/ReadVariableOp_43ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
~
&training/RMSprop/mul_21/ReadVariableOpReadVariableOptraining/RMSprop/Variable_7*
dtype0*
_output_shapes
: 

training/RMSprop/mul_21Mul"training/RMSprop/ReadVariableOp_43&training/RMSprop/mul_21/ReadVariableOp*
_output_shapes
: *
T0
f
"training/RMSprop/ReadVariableOp_44ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
^
training/RMSprop/sub_14/xConst*
dtype0*
_output_shapes
: *
valueB
 *  ?
~
training/RMSprop/sub_14Subtraining/RMSprop/sub_14/x"training/RMSprop/ReadVariableOp_44*
T0*
_output_shapes
: 

training/RMSprop/Square_7Square;training/RMSprop/gradients/dense_2/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
: 
w
training/RMSprop/mul_22Multraining/RMSprop/sub_14training/RMSprop/Square_7*
T0*
_output_shapes
: 
u
training/RMSprop/add_14Addtraining/RMSprop/mul_21training/RMSprop/mul_22*
T0*
_output_shapes
: 
{
$training/RMSprop/AssignVariableOp_14AssignVariableOptraining/RMSprop/Variable_7training/RMSprop/add_14*
dtype0
Ą
"training/RMSprop/ReadVariableOp_45ReadVariableOptraining/RMSprop/Variable_7%^training/RMSprop/AssignVariableOp_14*
dtype0*
_output_shapes
: 
e
"training/RMSprop/ReadVariableOp_46ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
¤
training/RMSprop/mul_23Mul"training/RMSprop/ReadVariableOp_46;training/RMSprop/gradients/dense_2/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
: 
^
training/RMSprop/Const_15Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_16Const*
dtype0*
_output_shapes
: *
valueB
 *  

(training/RMSprop/clip_by_value_7/MinimumMinimumtraining/RMSprop/add_14training/RMSprop/Const_16*
T0*
_output_shapes
: 

 training/RMSprop/clip_by_value_7Maximum(training/RMSprop/clip_by_value_7/Minimumtraining/RMSprop/Const_15*
T0*
_output_shapes
: 
f
training/RMSprop/Sqrt_7Sqrt training/RMSprop/clip_by_value_7*
T0*
_output_shapes
: 
^
training/RMSprop/add_15/yConst*
dtype0*
_output_shapes
: *
valueB
 *żÖ3
w
training/RMSprop/add_15Addtraining/RMSprop/Sqrt_7training/RMSprop/add_15/y*
_output_shapes
: *
T0
|
training/RMSprop/truediv_7RealDivtraining/RMSprop/mul_23training/RMSprop/add_15*
T0*
_output_shapes
: 
k
"training/RMSprop/ReadVariableOp_47ReadVariableOpdense_2/bias*
dtype0*
_output_shapes
: 

training/RMSprop/sub_15Sub"training/RMSprop/ReadVariableOp_47training/RMSprop/truediv_7*
T0*
_output_shapes
: 
l
$training/RMSprop/AssignVariableOp_15AssignVariableOpdense_2/biastraining/RMSprop/sub_15*
dtype0

"training/RMSprop/ReadVariableOp_48ReadVariableOpdense_2/bias%^training/RMSprop/AssignVariableOp_15*
dtype0*
_output_shapes
: 
f
"training/RMSprop/ReadVariableOp_49ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 

&training/RMSprop/mul_24/ReadVariableOpReadVariableOptraining/RMSprop/Variable_8*
dtype0*
_output_shapes

: 

training/RMSprop/mul_24Mul"training/RMSprop/ReadVariableOp_49&training/RMSprop/mul_24/ReadVariableOp*
T0*
_output_shapes

: 
f
"training/RMSprop/ReadVariableOp_50ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
^
training/RMSprop/sub_16/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: 
~
training/RMSprop/sub_16Subtraining/RMSprop/sub_16/x"training/RMSprop/ReadVariableOp_50*
T0*
_output_shapes
: 

training/RMSprop/Square_8Square7training/RMSprop/gradients/dense_3/MatMul_grad/MatMul_1*
T0*
_output_shapes

: 
{
training/RMSprop/mul_25Multraining/RMSprop/sub_16training/RMSprop/Square_8*
T0*
_output_shapes

: 
y
training/RMSprop/add_16Addtraining/RMSprop/mul_24training/RMSprop/mul_25*
T0*
_output_shapes

: 
{
$training/RMSprop/AssignVariableOp_16AssignVariableOptraining/RMSprop/Variable_8training/RMSprop/add_16*
dtype0
Ľ
"training/RMSprop/ReadVariableOp_51ReadVariableOptraining/RMSprop/Variable_8%^training/RMSprop/AssignVariableOp_16*
dtype0*
_output_shapes

: 
e
"training/RMSprop/ReadVariableOp_52ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
¤
training/RMSprop/mul_26Mul"training/RMSprop/ReadVariableOp_527training/RMSprop/gradients/dense_3/MatMul_grad/MatMul_1*
_output_shapes

: *
T0
^
training/RMSprop/Const_17Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_18Const*
valueB
 *  *
dtype0*
_output_shapes
: 

(training/RMSprop/clip_by_value_8/MinimumMinimumtraining/RMSprop/add_16training/RMSprop/Const_18*
T0*
_output_shapes

: 

 training/RMSprop/clip_by_value_8Maximum(training/RMSprop/clip_by_value_8/Minimumtraining/RMSprop/Const_17*
_output_shapes

: *
T0
j
training/RMSprop/Sqrt_8Sqrt training/RMSprop/clip_by_value_8*
T0*
_output_shapes

: 
^
training/RMSprop/add_17/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
{
training/RMSprop/add_17Addtraining/RMSprop/Sqrt_8training/RMSprop/add_17/y*
T0*
_output_shapes

: 

training/RMSprop/truediv_8RealDivtraining/RMSprop/mul_26training/RMSprop/add_17*
_output_shapes

: *
T0
q
"training/RMSprop/ReadVariableOp_53ReadVariableOpdense_3/kernel*
dtype0*
_output_shapes

: 

training/RMSprop/sub_17Sub"training/RMSprop/ReadVariableOp_53training/RMSprop/truediv_8*
_output_shapes

: *
T0
n
$training/RMSprop/AssignVariableOp_17AssignVariableOpdense_3/kerneltraining/RMSprop/sub_17*
dtype0

"training/RMSprop/ReadVariableOp_54ReadVariableOpdense_3/kernel%^training/RMSprop/AssignVariableOp_17*
dtype0*
_output_shapes

: 
f
"training/RMSprop/ReadVariableOp_55ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
~
&training/RMSprop/mul_27/ReadVariableOpReadVariableOptraining/RMSprop/Variable_9*
dtype0*
_output_shapes
:

training/RMSprop/mul_27Mul"training/RMSprop/ReadVariableOp_55&training/RMSprop/mul_27/ReadVariableOp*
T0*
_output_shapes
:
f
"training/RMSprop/ReadVariableOp_56ReadVariableOpRMSprop/rho*
dtype0*
_output_shapes
: 
^
training/RMSprop/sub_18/xConst*
dtype0*
_output_shapes
: *
valueB
 *  ?
~
training/RMSprop/sub_18Subtraining/RMSprop/sub_18/x"training/RMSprop/ReadVariableOp_56*
T0*
_output_shapes
: 

training/RMSprop/Square_9Square;training/RMSprop/gradients/dense_3/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:
w
training/RMSprop/mul_28Multraining/RMSprop/sub_18training/RMSprop/Square_9*
_output_shapes
:*
T0
u
training/RMSprop/add_18Addtraining/RMSprop/mul_27training/RMSprop/mul_28*
T0*
_output_shapes
:
{
$training/RMSprop/AssignVariableOp_18AssignVariableOptraining/RMSprop/Variable_9training/RMSprop/add_18*
dtype0
Ą
"training/RMSprop/ReadVariableOp_57ReadVariableOptraining/RMSprop/Variable_9%^training/RMSprop/AssignVariableOp_18*
dtype0*
_output_shapes
:
e
"training/RMSprop/ReadVariableOp_58ReadVariableOp
RMSprop/lr*
dtype0*
_output_shapes
: 
¤
training/RMSprop/mul_29Mul"training/RMSprop/ReadVariableOp_58;training/RMSprop/gradients/dense_3/BiasAdd_grad/BiasAddGrad*
T0*
_output_shapes
:
^
training/RMSprop/Const_19Const*
valueB
 *    *
dtype0*
_output_shapes
: 
^
training/RMSprop/Const_20Const*
dtype0*
_output_shapes
: *
valueB
 *  

(training/RMSprop/clip_by_value_9/MinimumMinimumtraining/RMSprop/add_18training/RMSprop/Const_20*
T0*
_output_shapes
:

 training/RMSprop/clip_by_value_9Maximum(training/RMSprop/clip_by_value_9/Minimumtraining/RMSprop/Const_19*
T0*
_output_shapes
:
f
training/RMSprop/Sqrt_9Sqrt training/RMSprop/clip_by_value_9*
T0*
_output_shapes
:
^
training/RMSprop/add_19/yConst*
valueB
 *żÖ3*
dtype0*
_output_shapes
: 
w
training/RMSprop/add_19Addtraining/RMSprop/Sqrt_9training/RMSprop/add_19/y*
T0*
_output_shapes
:
|
training/RMSprop/truediv_9RealDivtraining/RMSprop/mul_29training/RMSprop/add_19*
_output_shapes
:*
T0
k
"training/RMSprop/ReadVariableOp_59ReadVariableOpdense_3/bias*
dtype0*
_output_shapes
:

training/RMSprop/sub_19Sub"training/RMSprop/ReadVariableOp_59training/RMSprop/truediv_9*
T0*
_output_shapes
:
l
$training/RMSprop/AssignVariableOp_19AssignVariableOpdense_3/biastraining/RMSprop/sub_19*
dtype0

"training/RMSprop/ReadVariableOp_60ReadVariableOpdense_3/bias%^training/RMSprop/AssignVariableOp_19*
dtype0*
_output_shapes
:
ź
training/group_depsNoOp	^loss/mul^metrics/acc/Mean ^training/RMSprop/ReadVariableOp#^training/RMSprop/ReadVariableOp_12#^training/RMSprop/ReadVariableOp_15#^training/RMSprop/ReadVariableOp_18#^training/RMSprop/ReadVariableOp_21#^training/RMSprop/ReadVariableOp_24#^training/RMSprop/ReadVariableOp_27"^training/RMSprop/ReadVariableOp_3#^training/RMSprop/ReadVariableOp_30#^training/RMSprop/ReadVariableOp_33#^training/RMSprop/ReadVariableOp_36#^training/RMSprop/ReadVariableOp_39#^training/RMSprop/ReadVariableOp_42#^training/RMSprop/ReadVariableOp_45#^training/RMSprop/ReadVariableOp_48#^training/RMSprop/ReadVariableOp_51#^training/RMSprop/ReadVariableOp_54#^training/RMSprop/ReadVariableOp_57"^training/RMSprop/ReadVariableOp_6#^training/RMSprop/ReadVariableOp_60"^training/RMSprop/ReadVariableOp_9
`
VarIsInitializedOp_10VarIsInitializedOptraining/RMSprop/Variable_5*
_output_shapes
: 
R
VarIsInitializedOp_11VarIsInitializedOpRMSprop/decay*
_output_shapes
: 
`
VarIsInitializedOp_12VarIsInitializedOptraining/RMSprop/Variable_1*
_output_shapes
: 
`
VarIsInitializedOp_13VarIsInitializedOptraining/RMSprop/Variable_7*
_output_shapes
: 
P
VarIsInitializedOp_14VarIsInitializedOpRMSprop/rho*
_output_shapes
: 
`
VarIsInitializedOp_15VarIsInitializedOptraining/RMSprop/Variable_9*
_output_shapes
: 
`
VarIsInitializedOp_16VarIsInitializedOptraining/RMSprop/Variable_6*
_output_shapes
: 
^
VarIsInitializedOp_17VarIsInitializedOptraining/RMSprop/Variable*
_output_shapes
: 
`
VarIsInitializedOp_18VarIsInitializedOptraining/RMSprop/Variable_8*
_output_shapes
: 
`
VarIsInitializedOp_19VarIsInitializedOptraining/RMSprop/Variable_2*
_output_shapes
: 
W
VarIsInitializedOp_20VarIsInitializedOpRMSprop/iterations*
_output_shapes
: 
`
VarIsInitializedOp_21VarIsInitializedOptraining/RMSprop/Variable_3*
_output_shapes
: 
`
VarIsInitializedOp_22VarIsInitializedOptraining/RMSprop/Variable_4*
_output_shapes
: 
O
VarIsInitializedOp_23VarIsInitializedOp
RMSprop/lr*
_output_shapes
: 
Ú
init_1NoOp^RMSprop/decay/Assign^RMSprop/iterations/Assign^RMSprop/lr/Assign^RMSprop/rho/Assign!^training/RMSprop/Variable/Assign#^training/RMSprop/Variable_1/Assign#^training/RMSprop/Variable_2/Assign#^training/RMSprop/Variable_3/Assign#^training/RMSprop/Variable_4/Assign#^training/RMSprop/Variable_5/Assign#^training/RMSprop/Variable_6/Assign#^training/RMSprop/Variable_7/Assign#^training/RMSprop/Variable_8/Assign#^training/RMSprop/Variable_9/Assign
g
Placeholder_10Placeholder*
dtype0*"
_output_shapes
:@*
shape:@
_
AssignVariableOp_10AssignVariableOptraining/RMSprop/VariablePlaceholder_10*
dtype0

ReadVariableOp_10ReadVariableOptraining/RMSprop/Variable^AssignVariableOp_10*
dtype0*"
_output_shapes
:@
W
Placeholder_11Placeholder*
dtype0*
_output_shapes
:@*
shape:@
a
AssignVariableOp_11AssignVariableOptraining/RMSprop/Variable_1Placeholder_11*
dtype0

ReadVariableOp_11ReadVariableOptraining/RMSprop/Variable_1^AssignVariableOp_11*
dtype0*
_output_shapes
:@
g
Placeholder_12Placeholder*
dtype0*"
_output_shapes
:@@*
shape:@@
a
AssignVariableOp_12AssignVariableOptraining/RMSprop/Variable_2Placeholder_12*
dtype0

ReadVariableOp_12ReadVariableOptraining/RMSprop/Variable_2^AssignVariableOp_12*
dtype0*"
_output_shapes
:@@
W
Placeholder_13Placeholder*
shape:@*
dtype0*
_output_shapes
:@
a
AssignVariableOp_13AssignVariableOptraining/RMSprop/Variable_3Placeholder_13*
dtype0

ReadVariableOp_13ReadVariableOptraining/RMSprop/Variable_3^AssignVariableOp_13*
dtype0*
_output_shapes
:@
g
Placeholder_14Placeholder*
dtype0*"
_output_shapes
:@ *
shape:@ 
a
AssignVariableOp_14AssignVariableOptraining/RMSprop/Variable_4Placeholder_14*
dtype0

ReadVariableOp_14ReadVariableOptraining/RMSprop/Variable_4^AssignVariableOp_14*
dtype0*"
_output_shapes
:@ 
W
Placeholder_15Placeholder*
dtype0*
_output_shapes
: *
shape: 
a
AssignVariableOp_15AssignVariableOptraining/RMSprop/Variable_5Placeholder_15*
dtype0

ReadVariableOp_15ReadVariableOptraining/RMSprop/Variable_5^AssignVariableOp_15*
dtype0*
_output_shapes
: 
a
Placeholder_16Placeholder*
dtype0*
_output_shapes
:	ŕ *
shape:	ŕ 
a
AssignVariableOp_16AssignVariableOptraining/RMSprop/Variable_6Placeholder_16*
dtype0

ReadVariableOp_16ReadVariableOptraining/RMSprop/Variable_6^AssignVariableOp_16*
dtype0*
_output_shapes
:	ŕ 
W
Placeholder_17Placeholder*
shape: *
dtype0*
_output_shapes
: 
a
AssignVariableOp_17AssignVariableOptraining/RMSprop/Variable_7Placeholder_17*
dtype0

ReadVariableOp_17ReadVariableOptraining/RMSprop/Variable_7^AssignVariableOp_17*
dtype0*
_output_shapes
: 
_
Placeholder_18Placeholder*
dtype0*
_output_shapes

: *
shape
: 
a
AssignVariableOp_18AssignVariableOptraining/RMSprop/Variable_8Placeholder_18*
dtype0

ReadVariableOp_18ReadVariableOptraining/RMSprop/Variable_8^AssignVariableOp_18*
dtype0*
_output_shapes

: 
W
Placeholder_19Placeholder*
shape:*
dtype0*
_output_shapes
:
a
AssignVariableOp_19AssignVariableOptraining/RMSprop/Variable_9Placeholder_19*
dtype0

ReadVariableOp_19ReadVariableOptraining/RMSprop/Variable_9^AssignVariableOp_19*
dtype0*
_output_shapes
:
P

save/ConstConst*
dtype0*
_output_shapes
: *
valueB Bmodel

save/StringJoin/inputs_1Const*<
value3B1 B+_temp_59be8df2d42748b2a145fee1662a7fdb/part*
dtype0*
_output_shapes
: 
u
save/StringJoin
StringJoin
save/Constsave/StringJoin/inputs_1*
N*
_output_shapes
: *
	separator 
Q
save/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
\
save/ShardedFilename/shardConst*
dtype0*
_output_shapes
: *
value	B : 
}
save/ShardedFilenameShardedFilenamesave/StringJoinsave/ShardedFilename/shardsave/num_shards*
_output_shapes
: 
Ú
save/SaveV2/tensor_namesConst*
valueBBRMSprop/decayBRMSprop/iterationsB
RMSprop/lrBRMSprop/rhoBconv1d_3/biasBconv1d_3/kernelBconv1d_4/biasBconv1d_4/kernelBconv1d_5/biasBconv1d_5/kernelBdense_2/biasBdense_2/kernelBdense_3/biasBdense_3/kernelBtraining/RMSprop/VariableBtraining/RMSprop/Variable_1Btraining/RMSprop/Variable_2Btraining/RMSprop/Variable_3Btraining/RMSprop/Variable_4Btraining/RMSprop/Variable_5Btraining/RMSprop/Variable_6Btraining/RMSprop/Variable_7Btraining/RMSprop/Variable_8Btraining/RMSprop/Variable_9*
dtype0*
_output_shapes
:

save/SaveV2/shape_and_slicesConst*C
value:B8B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:
ă
save/SaveV2SaveV2save/ShardedFilenamesave/SaveV2/tensor_namessave/SaveV2/shape_and_slices!RMSprop/decay/Read/ReadVariableOp&RMSprop/iterations/Read/ReadVariableOpRMSprop/lr/Read/ReadVariableOpRMSprop/rho/Read/ReadVariableOp!conv1d_3/bias/Read/ReadVariableOp#conv1d_3/kernel/Read/ReadVariableOp!conv1d_4/bias/Read/ReadVariableOp#conv1d_4/kernel/Read/ReadVariableOp!conv1d_5/bias/Read/ReadVariableOp#conv1d_5/kernel/Read/ReadVariableOp dense_2/bias/Read/ReadVariableOp"dense_2/kernel/Read/ReadVariableOp dense_3/bias/Read/ReadVariableOp"dense_3/kernel/Read/ReadVariableOp-training/RMSprop/Variable/Read/ReadVariableOp/training/RMSprop/Variable_1/Read/ReadVariableOp/training/RMSprop/Variable_2/Read/ReadVariableOp/training/RMSprop/Variable_3/Read/ReadVariableOp/training/RMSprop/Variable_4/Read/ReadVariableOp/training/RMSprop/Variable_5/Read/ReadVariableOp/training/RMSprop/Variable_6/Read/ReadVariableOp/training/RMSprop/Variable_7/Read/ReadVariableOp/training/RMSprop/Variable_8/Read/ReadVariableOp/training/RMSprop/Variable_9/Read/ReadVariableOp*&
dtypes
2	

save/control_dependencyIdentitysave/ShardedFilename^save/SaveV2*
_output_shapes
: *
T0*'
_class
loc:@save/ShardedFilename

+save/MergeV2Checkpoints/checkpoint_prefixesPacksave/ShardedFilename^save/control_dependency*
T0*

axis *
N*
_output_shapes
:
}
save/MergeV2CheckpointsMergeV2Checkpoints+save/MergeV2Checkpoints/checkpoint_prefixes
save/Const*
delete_old_dirs(
z
save/IdentityIdentity
save/Const^save/MergeV2Checkpoints^save/control_dependency*
T0*
_output_shapes
: 
Ý
save/RestoreV2/tensor_namesConst*
valueBBRMSprop/decayBRMSprop/iterationsB
RMSprop/lrBRMSprop/rhoBconv1d_3/biasBconv1d_3/kernelBconv1d_4/biasBconv1d_4/kernelBconv1d_5/biasBconv1d_5/kernelBdense_2/biasBdense_2/kernelBdense_3/biasBdense_3/kernelBtraining/RMSprop/VariableBtraining/RMSprop/Variable_1Btraining/RMSprop/Variable_2Btraining/RMSprop/Variable_3Btraining/RMSprop/Variable_4Btraining/RMSprop/Variable_5Btraining/RMSprop/Variable_6Btraining/RMSprop/Variable_7Btraining/RMSprop/Variable_8Btraining/RMSprop/Variable_9*
dtype0*
_output_shapes
:

save/RestoreV2/shape_and_slicesConst*C
value:B8B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0*
_output_shapes
:

save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices*t
_output_shapesb
`::::::::::::::::::::::::*&
dtypes
2	
N
save/Identity_1Identitysave/RestoreV2*
T0*
_output_shapes
:
V
save/AssignVariableOpAssignVariableOpRMSprop/decaysave/Identity_1*
dtype0
P
save/Identity_2Identitysave/RestoreV2:1*
_output_shapes
:*
T0	
]
save/AssignVariableOp_1AssignVariableOpRMSprop/iterationssave/Identity_2*
dtype0	
P
save/Identity_3Identitysave/RestoreV2:2*
T0*
_output_shapes
:
U
save/AssignVariableOp_2AssignVariableOp
RMSprop/lrsave/Identity_3*
dtype0
P
save/Identity_4Identitysave/RestoreV2:3*
T0*
_output_shapes
:
V
save/AssignVariableOp_3AssignVariableOpRMSprop/rhosave/Identity_4*
dtype0
P
save/Identity_5Identitysave/RestoreV2:4*
_output_shapes
:*
T0
X
save/AssignVariableOp_4AssignVariableOpconv1d_3/biassave/Identity_5*
dtype0
P
save/Identity_6Identitysave/RestoreV2:5*
T0*
_output_shapes
:
Z
save/AssignVariableOp_5AssignVariableOpconv1d_3/kernelsave/Identity_6*
dtype0
P
save/Identity_7Identitysave/RestoreV2:6*
_output_shapes
:*
T0
X
save/AssignVariableOp_6AssignVariableOpconv1d_4/biassave/Identity_7*
dtype0
P
save/Identity_8Identitysave/RestoreV2:7*
T0*
_output_shapes
:
Z
save/AssignVariableOp_7AssignVariableOpconv1d_4/kernelsave/Identity_8*
dtype0
P
save/Identity_9Identitysave/RestoreV2:8*
T0*
_output_shapes
:
X
save/AssignVariableOp_8AssignVariableOpconv1d_5/biassave/Identity_9*
dtype0
Q
save/Identity_10Identitysave/RestoreV2:9*
_output_shapes
:*
T0
[
save/AssignVariableOp_9AssignVariableOpconv1d_5/kernelsave/Identity_10*
dtype0
R
save/Identity_11Identitysave/RestoreV2:10*
T0*
_output_shapes
:
Y
save/AssignVariableOp_10AssignVariableOpdense_2/biassave/Identity_11*
dtype0
R
save/Identity_12Identitysave/RestoreV2:11*
T0*
_output_shapes
:
[
save/AssignVariableOp_11AssignVariableOpdense_2/kernelsave/Identity_12*
dtype0
R
save/Identity_13Identitysave/RestoreV2:12*
T0*
_output_shapes
:
Y
save/AssignVariableOp_12AssignVariableOpdense_3/biassave/Identity_13*
dtype0
R
save/Identity_14Identitysave/RestoreV2:13*
_output_shapes
:*
T0
[
save/AssignVariableOp_13AssignVariableOpdense_3/kernelsave/Identity_14*
dtype0
R
save/Identity_15Identitysave/RestoreV2:14*
T0*
_output_shapes
:
f
save/AssignVariableOp_14AssignVariableOptraining/RMSprop/Variablesave/Identity_15*
dtype0
R
save/Identity_16Identitysave/RestoreV2:15*
_output_shapes
:*
T0
h
save/AssignVariableOp_15AssignVariableOptraining/RMSprop/Variable_1save/Identity_16*
dtype0
R
save/Identity_17Identitysave/RestoreV2:16*
T0*
_output_shapes
:
h
save/AssignVariableOp_16AssignVariableOptraining/RMSprop/Variable_2save/Identity_17*
dtype0
R
save/Identity_18Identitysave/RestoreV2:17*
T0*
_output_shapes
:
h
save/AssignVariableOp_17AssignVariableOptraining/RMSprop/Variable_3save/Identity_18*
dtype0
R
save/Identity_19Identitysave/RestoreV2:18*
T0*
_output_shapes
:
h
save/AssignVariableOp_18AssignVariableOptraining/RMSprop/Variable_4save/Identity_19*
dtype0
R
save/Identity_20Identitysave/RestoreV2:19*
_output_shapes
:*
T0
h
save/AssignVariableOp_19AssignVariableOptraining/RMSprop/Variable_5save/Identity_20*
dtype0
R
save/Identity_21Identitysave/RestoreV2:20*
T0*
_output_shapes
:
h
save/AssignVariableOp_20AssignVariableOptraining/RMSprop/Variable_6save/Identity_21*
dtype0
R
save/Identity_22Identitysave/RestoreV2:21*
T0*
_output_shapes
:
h
save/AssignVariableOp_21AssignVariableOptraining/RMSprop/Variable_7save/Identity_22*
dtype0
R
save/Identity_23Identitysave/RestoreV2:22*
T0*
_output_shapes
:
h
save/AssignVariableOp_22AssignVariableOptraining/RMSprop/Variable_8save/Identity_23*
dtype0
R
save/Identity_24Identitysave/RestoreV2:23*
T0*
_output_shapes
:
h
save/AssignVariableOp_23AssignVariableOptraining/RMSprop/Variable_9save/Identity_24*
dtype0

save/restore_shardNoOp^save/AssignVariableOp^save/AssignVariableOp_1^save/AssignVariableOp_10^save/AssignVariableOp_11^save/AssignVariableOp_12^save/AssignVariableOp_13^save/AssignVariableOp_14^save/AssignVariableOp_15^save/AssignVariableOp_16^save/AssignVariableOp_17^save/AssignVariableOp_18^save/AssignVariableOp_19^save/AssignVariableOp_2^save/AssignVariableOp_20^save/AssignVariableOp_21^save/AssignVariableOp_22^save/AssignVariableOp_23^save/AssignVariableOp_3^save/AssignVariableOp_4^save/AssignVariableOp_5^save/AssignVariableOp_6^save/AssignVariableOp_7^save/AssignVariableOp_8^save/AssignVariableOp_9
-
save/restore_allNoOp^save/restore_shard "<
save/Const:0save/Identity:0save/restore_all (5 @F8"Ţ
trainable_variablesĆĂ

conv1d_3/kernel:0conv1d_3/kernel/Assign%conv1d_3/kernel/Read/ReadVariableOp:0(2,conv1d_3/kernel/Initializer/random_uniform:08
s
conv1d_3/bias:0conv1d_3/bias/Assign#conv1d_3/bias/Read/ReadVariableOp:0(2!conv1d_3/bias/Initializer/zeros:08

conv1d_4/kernel:0conv1d_4/kernel/Assign%conv1d_4/kernel/Read/ReadVariableOp:0(2,conv1d_4/kernel/Initializer/random_uniform:08
s
conv1d_4/bias:0conv1d_4/bias/Assign#conv1d_4/bias/Read/ReadVariableOp:0(2!conv1d_4/bias/Initializer/zeros:08

conv1d_5/kernel:0conv1d_5/kernel/Assign%conv1d_5/kernel/Read/ReadVariableOp:0(2,conv1d_5/kernel/Initializer/random_uniform:08
s
conv1d_5/bias:0conv1d_5/bias/Assign#conv1d_5/bias/Read/ReadVariableOp:0(2!conv1d_5/bias/Initializer/zeros:08

dense_2/kernel:0dense_2/kernel/Assign$dense_2/kernel/Read/ReadVariableOp:0(2+dense_2/kernel/Initializer/random_uniform:08
o
dense_2/bias:0dense_2/bias/Assign"dense_2/bias/Read/ReadVariableOp:0(2 dense_2/bias/Initializer/zeros:08

dense_3/kernel:0dense_3/kernel/Assign$dense_3/kernel/Read/ReadVariableOp:0(2+dense_3/kernel/Initializer/random_uniform:08
o
dense_3/bias:0dense_3/bias/Assign"dense_3/bias/Read/ReadVariableOp:0(2 dense_3/bias/Initializer/zeros:08
o
RMSprop/lr:0RMSprop/lr/Assign RMSprop/lr/Read/ReadVariableOp:0(2&RMSprop/lr/Initializer/initial_value:08
s
RMSprop/rho:0RMSprop/rho/Assign!RMSprop/rho/Read/ReadVariableOp:0(2'RMSprop/rho/Initializer/initial_value:08
{
RMSprop/decay:0RMSprop/decay/Assign#RMSprop/decay/Read/ReadVariableOp:0(2)RMSprop/decay/Initializer/initial_value:08

RMSprop/iterations:0RMSprop/iterations/Assign(RMSprop/iterations/Read/ReadVariableOp:0(2.RMSprop/iterations/Initializer/initial_value:08

training/RMSprop/Variable:0 training/RMSprop/Variable/Assign/training/RMSprop/Variable/Read/ReadVariableOp:0(2training/RMSprop/zeros:08

training/RMSprop/Variable_1:0"training/RMSprop/Variable_1/Assign1training/RMSprop/Variable_1/Read/ReadVariableOp:0(2training/RMSprop/zeros_1:08

training/RMSprop/Variable_2:0"training/RMSprop/Variable_2/Assign1training/RMSprop/Variable_2/Read/ReadVariableOp:0(2training/RMSprop/zeros_2:08

training/RMSprop/Variable_3:0"training/RMSprop/Variable_3/Assign1training/RMSprop/Variable_3/Read/ReadVariableOp:0(2training/RMSprop/zeros_3:08

training/RMSprop/Variable_4:0"training/RMSprop/Variable_4/Assign1training/RMSprop/Variable_4/Read/ReadVariableOp:0(2training/RMSprop/zeros_4:08

training/RMSprop/Variable_5:0"training/RMSprop/Variable_5/Assign1training/RMSprop/Variable_5/Read/ReadVariableOp:0(2training/RMSprop/zeros_5:08

training/RMSprop/Variable_6:0"training/RMSprop/Variable_6/Assign1training/RMSprop/Variable_6/Read/ReadVariableOp:0(2training/RMSprop/zeros_6:08

training/RMSprop/Variable_7:0"training/RMSprop/Variable_7/Assign1training/RMSprop/Variable_7/Read/ReadVariableOp:0(2training/RMSprop/zeros_7:08

training/RMSprop/Variable_8:0"training/RMSprop/Variable_8/Assign1training/RMSprop/Variable_8/Read/ReadVariableOp:0(2training/RMSprop/zeros_8:08

training/RMSprop/Variable_9:0"training/RMSprop/Variable_9/Assign1training/RMSprop/Variable_9/Read/ReadVariableOp:0(2training/RMSprop/zeros_9:08"ąw
cond_context ww
Ú
Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/cond_textQloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_t:0 *Ű
Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar:0
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch_1:0
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch_1:1
Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_t:0Ś
Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0
Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar:0Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Switch_1:1
ëV
Sloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/cond_text_1Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_f:0*Ŕ'
iloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Merge:0
iloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Merge:1
jloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch:0
jloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch:1
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:2
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/dim:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/dim:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat/axis:0
{loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/num_invalid_dims:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Const:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Shape:0
~loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like:0
vloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/x:0
tloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims:0
wloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch:0
yloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch_1:0
ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank:0
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_f:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t:0
Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0
Rloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/switch_f:0
Floss/dense_3_loss/broadcast_weights/assert_broadcastable/values/rank:0
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rank:0
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0Ä
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/rank:0yloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch_1:0Á
Floss/dense_3_loss/broadcast_weights/assert_broadcastable/values/rank:0wloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank/Switch:0Ő
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch:0Ś
Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0Qloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/pred_id:0Ň
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch:02í"
ę"
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/cond_textkloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t:0 * 
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/DenseToDenseSetOperation:2
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch_1:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/dim:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch_1:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/dim:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat/axis:0
{loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/concat:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/num_invalid_dims:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Const:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like/Shape:0
~loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ones_like:0
vloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/x:0
tloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims:0
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_t:0
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0Ú
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0×
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch_1:1
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims_1/Switch:0
loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch:0Ô
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0loss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/has_invalid_dims/ExpandDims/Switch_1:12š

ś

mloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/cond_text_1kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_f:0*é
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1:1
ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank:0
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0
lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/switch_f:0Ú
kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0kloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/pred_id:0ŕ
ploss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/is_same_rank:0lloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/has_valid_nonscalar_shape/Switch_1:0

Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/cond_textNloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_t:0 *
Yloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependency:0
Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0
Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_t:0 
Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0
ˇ
Ploss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/cond_text_1Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f:0*Á
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch:0
Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_1:0
Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_2:0
Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_3:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_0:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_1:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_2:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_4:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_5:0
Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/data_7:0
[loss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/control_dependency_1:0
Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0
Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/switch_f:0
Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar:0
Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Merge:0
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0Ą
Gloss/dense_3_loss/broadcast_weights/assert_broadcastable/values/shape:0Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_2:0§
Oloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_valid_shape/Merge:0Tloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch:0˘
Hloss/dense_3_loss/broadcast_weights/assert_broadcastable/weights/shape:0Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_1:0 
Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0Nloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/pred_id:0
Dloss/dense_3_loss/broadcast_weights/assert_broadcastable/is_scalar:0Vloss/dense_3_loss/broadcast_weights/assert_broadcastable/AssertGuard/Assert/Switch_3:0"Ô
	variablesĆĂ

conv1d_3/kernel:0conv1d_3/kernel/Assign%conv1d_3/kernel/Read/ReadVariableOp:0(2,conv1d_3/kernel/Initializer/random_uniform:08
s
conv1d_3/bias:0conv1d_3/bias/Assign#conv1d_3/bias/Read/ReadVariableOp:0(2!conv1d_3/bias/Initializer/zeros:08

conv1d_4/kernel:0conv1d_4/kernel/Assign%conv1d_4/kernel/Read/ReadVariableOp:0(2,conv1d_4/kernel/Initializer/random_uniform:08
s
conv1d_4/bias:0conv1d_4/bias/Assign#conv1d_4/bias/Read/ReadVariableOp:0(2!conv1d_4/bias/Initializer/zeros:08

conv1d_5/kernel:0conv1d_5/kernel/Assign%conv1d_5/kernel/Read/ReadVariableOp:0(2,conv1d_5/kernel/Initializer/random_uniform:08
s
conv1d_5/bias:0conv1d_5/bias/Assign#conv1d_5/bias/Read/ReadVariableOp:0(2!conv1d_5/bias/Initializer/zeros:08

dense_2/kernel:0dense_2/kernel/Assign$dense_2/kernel/Read/ReadVariableOp:0(2+dense_2/kernel/Initializer/random_uniform:08
o
dense_2/bias:0dense_2/bias/Assign"dense_2/bias/Read/ReadVariableOp:0(2 dense_2/bias/Initializer/zeros:08

dense_3/kernel:0dense_3/kernel/Assign$dense_3/kernel/Read/ReadVariableOp:0(2+dense_3/kernel/Initializer/random_uniform:08
o
dense_3/bias:0dense_3/bias/Assign"dense_3/bias/Read/ReadVariableOp:0(2 dense_3/bias/Initializer/zeros:08
o
RMSprop/lr:0RMSprop/lr/Assign RMSprop/lr/Read/ReadVariableOp:0(2&RMSprop/lr/Initializer/initial_value:08
s
RMSprop/rho:0RMSprop/rho/Assign!RMSprop/rho/Read/ReadVariableOp:0(2'RMSprop/rho/Initializer/initial_value:08
{
RMSprop/decay:0RMSprop/decay/Assign#RMSprop/decay/Read/ReadVariableOp:0(2)RMSprop/decay/Initializer/initial_value:08

RMSprop/iterations:0RMSprop/iterations/Assign(RMSprop/iterations/Read/ReadVariableOp:0(2.RMSprop/iterations/Initializer/initial_value:08

training/RMSprop/Variable:0 training/RMSprop/Variable/Assign/training/RMSprop/Variable/Read/ReadVariableOp:0(2training/RMSprop/zeros:08

training/RMSprop/Variable_1:0"training/RMSprop/Variable_1/Assign1training/RMSprop/Variable_1/Read/ReadVariableOp:0(2training/RMSprop/zeros_1:08

training/RMSprop/Variable_2:0"training/RMSprop/Variable_2/Assign1training/RMSprop/Variable_2/Read/ReadVariableOp:0(2training/RMSprop/zeros_2:08

training/RMSprop/Variable_3:0"training/RMSprop/Variable_3/Assign1training/RMSprop/Variable_3/Read/ReadVariableOp:0(2training/RMSprop/zeros_3:08

training/RMSprop/Variable_4:0"training/RMSprop/Variable_4/Assign1training/RMSprop/Variable_4/Read/ReadVariableOp:0(2training/RMSprop/zeros_4:08

training/RMSprop/Variable_5:0"training/RMSprop/Variable_5/Assign1training/RMSprop/Variable_5/Read/ReadVariableOp:0(2training/RMSprop/zeros_5:08

training/RMSprop/Variable_6:0"training/RMSprop/Variable_6/Assign1training/RMSprop/Variable_6/Read/ReadVariableOp:0(2training/RMSprop/zeros_6:08

training/RMSprop/Variable_7:0"training/RMSprop/Variable_7/Assign1training/RMSprop/Variable_7/Read/ReadVariableOp:0(2training/RMSprop/zeros_7:08

training/RMSprop/Variable_8:0"training/RMSprop/Variable_8/Assign1training/RMSprop/Variable_8/Read/ReadVariableOp:0(2training/RMSprop/zeros_8:08

training/RMSprop/Variable_9:0"training/RMSprop/Variable_9/Assign1training/RMSprop/Variable_9/Read/ReadVariableOp:0(2training/RMSprop/zeros_9:08*Ź
serving_default
;
input_image,
conv1d_3_input:0˙˙˙˙˙˙˙˙˙ú=
dense_3/Softmax:0(
dense_3/Softmax:0˙˙˙˙˙˙˙˙˙tensorflow/serving/predict